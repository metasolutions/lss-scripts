const { EntryStore } = require('@entryscape/entrystore-js');
const { create } = require('xmlbuilder2');
const { publisherOrgNrLookup, themeDict } = require('./dicts');
const repository = 'https://catalog.lansstyrelsen.se/store/';
const MAX_AGE = 86400;
const es = new EntryStore(repository);
const nameArr = require('./names.js');
const  listOfNames = nameArr.nameArray;

let listOfFailedUpdates = {};


const isValidIsbn = function(str) {

  var sum,
      weight,
      digit,
      check,
      i;

  str = str.replace(/[^0-9X]/gi, '');

  if (str.length != 10 && str.length != 13) {
      return false;
  }

  if (str.length == 13) {
      sum = 0;
      for (i = 0; i < 12; i++) {
          digit = parseInt(str[i]);
          if (i % 2 == 1) {
              sum += 3*digit;
          } else {
              sum += digit;
          }
      }
      check = (10 - (sum % 10)) % 10;
      return (check == str[str.length-1]);
  }

  if (str.length == 10) {
      weight = 10;
      sum = 0;
      for (i = 0; i < 9; i++) {
          digit = parseInt(str[i]);
          sum += weight*digit;
          weight--;
      }
      check = (11 - (sum % 11)) % 11
      if (check == 10) {
          check = 'X';
      }
      return (check == str[str.length-1].toUpperCase());
  }
}

const getAppendices = async (md,item,ruri, modifiedDate) => {
  const appendices = md.find(ruri, 'dcterms:hasPart');
  if(!appendices) return;
  const promises = appendices.map(async (appendix) => {
    try{
      const appendixValue = appendix.getValue();
      const appendixURI =   await es.getEntryURIFromURI(appendixValue);
      const appendixEntry = await es.getEntry(appendixURI);
  
      const format = appendixEntry.getEntryInfo().getFormat();
  
      if(format === 'application/pdf'){
        if (!('media:content' in item)){
          item['media:content'] = [];
        }
        item['media:content'].push({
          '@url': appendixEntry.getResourceURI(),
          '@type': format
        });
      }
    } catch(err){
    //console.log(err);
    }
  })
  await Promise.all(promises);
}

const handlePDF = async (md, item, ruri, modifiedDate) => {
  //let versionURI = md.findFirstValue(ruri, 'dcterms:hasVersion');
  let versions = md.find(ruri, 'dcterms:hasVersion');
  const promises = versions.map(async (v) => {
    try{
      const versionURI = v.getValue();
      const entryURI =   await es.getEntryURIFromURI(versionURI);
      const versionEntry = await es.getEntry(entryURI);
  
      const versionsModifiedDate = versionEntry.getEntryInfo().getModificationDate();
  
      if(versionsModifiedDate > modifiedDate){
        //let date = new Date(versionsModifiedDate);
        //item['pubDate'] = versionsModifiedDate.toUTCString();
      }
  
      const format = versionEntry.getEntryInfo().getFormat();
  
      if( format === 'application/pdf'){
        if (!("link" in item)){
          item['dcterms:format'] = 'application/pdf';
          item['link'] = versionURI;
        } else {
          if (!('media:content' in item)){
            item['media:content'] = [];
          }
          item['media:content'].push({
            '@url': versionURI,
            '@type': format
          });
        }
      }
    }  catch(err){
      //console.log(err);
    }
  })

  await Promise.all(promises);
}

const createObject = async (entry, publisherObjects) => {

  const md = entry.getMetadata();
  const item = {};
  let flag = 0; // if flag set to one, then item is not valid

  const title = md.find(entry.getResourceURI(), 'dcterms:title');
  if (title[0] !== undefined) {

    let swedishTitle;
    let englishTitle;
    for (t of title) {
      const language = t["_o"]["lang"];
      if (language === 'sv') {
        swedishTitle = t.getValue().replace(/\s{2,}/g, ' ').trim();
      } else if (language === 'en') {
        englishTitle = t.getValue().replace(/\s{2,}/g, ' ').trim();
      }
    }
    if (swedishTitle) {
      item['title'] = swedishTitle;
    } else if (englishTitle)  {
      item['title'] = englishTitle;
    } else {
      item['title'] = title[0].getValue();
    }
  } else {
    flag = 1;
  };

 

  const ruri = entry.getResourceURI();
  item['guid'] = ruri;

  let isbn = md.findFirstValue(null, "http://schema.org/isbn");
  if (isbn != undefined) {
    isbn = isbn.replaceAll("-", "");
    if (isValidIsbn(isbn)) {
      if (!("dcterms:identifier" in item)) {
        item['dcterms:identifier'] = [];
      };
      item["dcterms:identifier"].push({
        '@xsi:type': 'dcterms:isbn',
        '#': isbn
      });
    }
  }

  
  let creator = md.findFirstValue(null, "http://purl.org/dc/terms/creator");
  if (creator != undefined){
    if (creator.includes(',')){
      creator = creator.split(',');
      for (const n of creator){
        let name = n.split(' ');
        let hasValidName = false;
        let firstNameIndex = 0;
        name = name.filter(item => item !== '');
     
        for (const n of name){
          if(listOfNames.includes(n)) {
            hasValidName = true;
            firstNameIndex = name.indexOf(n);
          };
        }
        if (!hasValidName || name.length>3){
          continue;
        };
        name = name.map((e) => {
          const onlyLetters = e.replace('.', '');
          return onlyLetters;
        });
        let firstname = name.shift();
        let surname = name.join(' ');
        name = surname.concat(', ', firstname);
        if(!('dcterms:creator' in item)) {
          item["dcterms:creator"] = [];
        }
        item["dcterms:creator"].push(name);
      }
    } else {
      creator = creator.split(' ');
      let hasValidName = false;
      let firstNameIndex = 0;
      for (const c of creator){
        if(listOfNames.includes(c)) {
          hasValidName = true;
          firstNameIndex = creator.indexOf(c);
        };
      }
      if(hasValidName && creator.length<=3) {
        creator = creator.map((e) => {
          const onlyLetters = e.replace('.', '');
          return onlyLetters;
        });
        let firstname = creator.shift();
        let surname = creator.join(' ');
        creator = surname.concat(', ', firstname);
        item["dcterms:creator"] = creator;
      }
    }
  }

  item['dcterms:accessRights'] = 'gratis';

  const issued = md.findFirstValue(null, 'dcterms:issued');


  let publisher = undefined;
  const publishers = md.find(null, 'dcterms:publisher');
  if (publishers[0] !== undefined) {

    if ( publishers.length === 1 && publishers[0].getValue() === 'http://data.lansstyrelsen.se/ns/county/all'){
      publisher = 'http://data.lansstyrelsen.se/ns/county/AB';
      item['dcterms:publisher'] = 'http://id.kb.se/organisations/SE' + '2021002247';
    } else {
      for (p of publishers) {
        let tempPublisher = p.getValue();
        if (publisherOrgNrLookup[tempPublisher] !== undefined ){
          publisher = tempPublisher;
          const  orgnr = publisherOrgNrLookup[tempPublisher]['org-nr'];
          item['dcterms:publisher'] = 'http://id.kb.se/organisations/SE' + orgnr;
          break;
        }
      }
    }
    if( !('dcterms:publisher' in item)){
      flag = 1;
    }
  } else {
    flag = 1;
  }


  const modifiedDate = entry.getEntryInfo().getModificationDate();
  if(modifiedDate !== undefined){
    item['kbse:versionFeedInsertDate'] = modifiedDate.toUTCString();
  }else {
    flag = 1;
  }

  const handleDescriptionString = (abstractString) => {
    let description = abstractString.replace(/\s+/g,' ').trim();
    description = description.replaceAll('&amp;', '');
    description = description.replaceAll('&gt;', '');
    description = description.replaceAll('&lt;', '');
    description = description.replaceAll('&quot;', '');
    description = description.replaceAll('&#39;', '');
    description = description.replaceAll('&', '');

    if (description.length>800){
      // If description is too wordy
      description = description.substring(0,800); 
    }
    description = description.substring(0, description.lastIndexOf('.')+1);

    return description;
  }

  const abstract = md.find(ruri, 'dcterms:abstract');
  if (abstract[0] !== undefined) {
    let swedishAbstract;
    let englishAbstract;
    for (a of abstract) {
      const language = a["_o"]["lang"];
      if (language == 'sv' ) {      
        swedishAbstract = handleDescriptionString(a.getValue());;
      } else if ( language == 'en') {
        englishAbstract = handleDescriptionString(a.getValue());;
      }
    }
    if (swedishAbstract) {
      item['description'] = swedishAbstract
    } else if (englishAbstract) {
      item['description'] = englishAbstract
    } else {
      const unknownLanguage = handleDescriptionString(abstract[0].getValue());
      item['description'] = unknownLanguage;
    }
  }

  const themes = md.find(ruri, "http://purl.org/dc/terms/subject");
  if (themes[0] != undefined) {
    if ( ! ("media:keywords" in item)){item["media:keywords"] = []};
    for (t of themes) {
      if ( t.getValue() in themeDict){
        item["media:keywords"].push(themeDict[t.getValue()]);
      }
    }
  };

  const available = md.findFirstValue(ruri, 'dcterms:available');
  if (available !== undefined) {
    // item["kbse:versionFeedInsertDate"] = available;
    const date = new Date(available);

    item['pubDate'] = date.toUTCString();
  };

  await handlePDF(md,item, ruri, modifiedDate);
  await getAppendices(md,item, ruri, modifiedDate);
  
  // If not, then the report is not a pdf
  if ('dcterms:format' in item && issued) {
    // Check mandatory fields before push
    let missingFields = [];
    const mandatoryFields = ['title', 'link', 'guid', 'dcterms:accessRights', 'dcterms:publisher', 'pubDate', 'dcterms:format', 'kbse:versionFeedInsertDate'];
    for (mandatoryF of mandatoryFields) {
      
      if (!(mandatoryF in item)) {
        flag = 1;
        missingFields.push(mandatoryF);
        if ( ! (ruri in listOfFailedUpdates) ){ 
          listOfFailedUpdates[ruri] = {ruri: ruri, title: item['title'], item:item, missing: missingFields};
        }
      }
    };

    if (flag) {
      //dont push
      //throw "Item with RURI " + ruri + " not included due to missing mandatory fields";
    } else {
      // push
      publisherObjects[publisher].push(item);
    }
  }
};

  


const createXML = (publisherUri, items, publisherXmlArr) => {

  const organisation = publisherOrgNrLookup[publisherUri]['county'];
  const title = "Länsstyrelsen " + organisation + " - Pliktleverans av elektroniskt material";

  
  let obj = {

      "rss": {
        '@version': '2.0',
        '@xmlns:dcterms': 'http://purl.org/dc/terms/',
        '@xmlns:media': 'http://search.yahoo.com/mrss/',
        '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        '@xmlns:kbse': 'http://www.kb.se/namespace/',
        'channel':  {
          'title': title,
          'language': 'sv'
          }
        }
      
  };

  const doc = create(obj);
  let xml = doc.end();

  if( items.length != 0){
    items.sort(function (a, b) {
      return new Date(b.pubDate) - new Date(a.pubDate);
    });

    const doc2 = create(xml);
    items.forEach(e => {
      doc2.root('rss').first()
        .ele('item')
        .ele(e).up();

    });
    xml = doc2.end({ prettyPrint: true });
  }
  

  // updates of each county
  if (!(Object.keys(items).length === 0)){
    console.log(items.length + " nya uppdateringar för Länsstyrelsen " + publisherOrgNrLookup[publisherUri]["county"]);
  }else{
    console.log(0 + " nya uppdateringar för Länsstyrelsen " + publisherOrgNrLookup[publisherUri]["county"]);
  }

  publisherXmlArr[publisherUri] = xml;
};

/*
* Remove pdfs that has a connection to a report
*
*/
const compareReports2PDFS = (reports, pdfs) => {
  reports.map( (report) =>{
    const versions = report.getMetadata().find(report.getResourceURI(), 'dcterms:hasVersion');
    for ( v of versions){
      const versionRURI = v.getValue();
      if ( pdfs.includes(versionRURI) ){
        const index = pdfs.indexOf(versionRURI);
        pdfs.splice(index, 1);
      }
    }
  })
  return pdfs;
}

const toDateRange = (from, to) => `[${from ? from.toISOString() : '*'} TO ${to ? to.toISOString() : '*'}]`;

const fetchUpdates = async (from, to) => {

  let publisherXml = {};
  let publisherObjects = {
    'http://data.lansstyrelsen.se/ns/county/BD': [], 
    'http://data.lansstyrelsen.se/ns/county/K': [], 
    'http://data.lansstyrelsen.se/ns/county/AB': [], 
    'http://data.lansstyrelsen.se/ns/county/X':  [], 
    'http://data.lansstyrelsen.se/ns/county/O':  [], 
    'http://data.lansstyrelsen.se/ns/county/M':  [], 
    'http://data.lansstyrelsen.se/ns/county/N': [], 
    'http://data.lansstyrelsen.se/ns/county/AC': [], 
    'http://data.lansstyrelsen.se/ns/county/I': [], 
    'http://data.lansstyrelsen.se/ns/county/F': [], 
    'http://data.lansstyrelsen.se/ns/county/W': [], 
    'http://data.lansstyrelsen.se/ns/county/T': [], 
    'http://data.lansstyrelsen.se/ns/county/Z': [],  
    'http://data.lansstyrelsen.se/ns/county/Y': [], 
    'http://data.lansstyrelsen.se/ns/county/U': [],  
    'http://data.lansstyrelsen.se/ns/county/S': [], 
    'http://data.lansstyrelsen.se/ns/county/H': [], 
    'http://data.lansstyrelsen.se/ns/county/G': [], 
    'http://data.lansstyrelsen.se/ns/county/E': [], 
    'http://data.lansstyrelsen.se/ns/county/D': [], 
    'http://data.lansstyrelsen.se/ns/county/C':[], 
  };

  const reportupdates = [];
  const pdfupdates = [];



  const dateRange = toDateRange(from,to);

  try {
    const queryReport =  es.newSolrQuery().modified(dateRange).rdfType('http\://data.lansstyrelsen.se/ns/Report');
    await queryReport.forEach(entry => {
      reportupdates.push(entry);
    });
  } catch (err) {
    console.log(err);
  }

  console.log('Found ' + reportupdates.length + ' reports');
  
  /*
  const queryPDF =  es.newSolrQuery().modified(dateRange).rdfType('http://schema.org/DigitalDocument');
  await queryPDF.forEach(entry => {
    const format = entry.getEntryInfo().getFormat();
    if( format == 'application/pdf'){
      pdfupdates.push(entry.getResourceURI());
    }
  });

  console.log('Found ' + pdfupdates.length + ' pdf documents');

  const pdfs2update = compareReports2PDFS(reportupdates, pdfupdates);


  console.log(pdfs2update.length + ' left after filtering');


  
  // Find the reports that have the filtered pdfs as object, add to list if it is a report
  try{
    const addReports = pdfs2update.map( async (pdf) => {
      const query =  es.newSolrQuery().uriProperty('dcterms:hasVersion', pdf).uriProperty('dcterms:hasPart', pdf).disjunctiveProperties()
      await query.forEach(entry => {
        const type = entry.getMetadata().findFirstValue(null, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        if (type === "http://data.lansstyrelsen.se/ns/Report") reportupdates.push(entry);
      });
    })
    await Promise.all(addReports);
  } catch (err) {
    console.log(err);
  }

  console.log('Reports after added pdfs: ' + reportupdates.length);
  
  */
  const uniqueArray = reportupdates.filter(function(item, pos) {
    return reportupdates.indexOf(item) == pos;
  });

  // console.log("unique: " + uniqueArray.length);

  for (const entry of uniqueArray) {
    await createObject(entry, publisherObjects);
  }

  for (const [key, value] of Object.entries(publisherObjects)) {
    if(value != []){
      createXML(key, value, publisherXml);
    }
  };

  return {'xml':publisherXml, "failed": listOfFailedUpdates};
};


exports.fetchUpdates = fetchUpdates;
exports.publisherOrgNrLookup = publisherOrgNrLookup;



