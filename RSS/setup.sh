#!/bin/bash

if ! [[ -d "feeds" ]]
then
  mkdir "feeds"
fi

if ! [[ -d "feeds/export" ]]
then
  mkdir "feeds/export"
fi

if ! [[ -d "feeds/logs" ]]
then
  mkdir "feeds/logs"
fi

if ! [[ -d "feeds/logs/drafts" ]]
then
  mkdir "feeds/logs/drafts"
fi

if ! [[ -d "feeds/logs/errors" ]]
then
  mkdir "feeds/logs/errors"
fi


if ! [[ -d "feeds/logs/validations" ]]
then
  mkdir "feeds/logs/validations"
fi

if ! [[ -d "log" ]]
then
  mkdir "log"
fi