Generating feeds designed for RSS. Fetch updated reports from the 7 days. The generated xml that containts items are then
validated using http://api.eplikt.kb.se/ . Logged results can be found in feeds/logs. Result then foun in feeds/export/

bash run.sh


Feeds are exported at https://catalog.lansstyrelsen.se/feed/<org-id>.xml

for instance https://catalog.lansstyrelsen.se/feed/2021002411.xml


Available organisations are:

2021002247 - Stockholm
2021002254 - Uppsala
2021002262 - Södermanland
2021002270 - Östergötland
2021002288 - Jönköping
2021002296 - Kronoberg
2021002304 - Kalmar
2021002312 - Gotland
2021002320 - Blekinge
2021002346 - Skåne
2021002353 - Halland
2021002361 - Västra Götaland
2021002395 - Värmland
2021002403 - Örebro
2021002411 - Västmanland
2021002429 - Dalarna
2021002437 - Gävleborg
2021002445 - Västernorrland
2021002452 - Jämtland
2021002460 - Västerbotten
2021002478 - Norrbotten