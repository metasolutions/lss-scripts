#!/bin/bash

# NOTE: Print output is sent in email

DATE=$(date '+%Y%m%d')

if [[ -d "feeds/logs/errors/${DATE}" ]]
then
  rm -r "feeds/logs/errors/${DATE}"
  mkdir "feeds/logs/errors/${DATE}"
else
  mkdir "feeds/logs/errors/${DATE}"
fi

if [[ -d "feeds/logs/errors/${DATE}" ]]
then
  rm -r "feeds/logs/errors/${DATE}"
  mkdir "feeds/logs/errors/${DATE}"
else
  mkdir "feeds/logs/errors/${DATE}"
fi

if [[ -d "feeds/logs/validations/${DATE}" ]]
then
  rm -r "feeds/logs/validations/${DATE}"
  mkdir "feeds/logs/validations/${DATE}"
else
  mkdir "feeds/logs/validations/${DATE}"
fi

node --trace-warnings src/main.js > log/log-${DATE}.txt 2> log/err-${DATE}.txt

for FILE in feeds/logs/drafts/${DATE}/*
do 
  # only check feeds that contains items
  if grep -q item "$FILE"
  then
    VALIDATION=$(curl -s --header "Content-Type:text/xml" --data-binary @$FILE "http://api.eplikt.kb.se/validate?schema=WEB_ARTICLE_RSS")
    if [[ $VALIDATION == *"ERROR"* ]]
    then
      echo File $FILE Not valid
      cp $FILE "feeds/logs/errors/${DATE}/"
    else
      #echo $FILE Valid
      cp $FILE "feeds/logs/validations/${DATE}/"
      cp $FILE "feeds/export/"
    fi
  else
    #echo $FILE Does not contain any items, added anyway
    cp $FILE "feeds/logs/validations/${DATE}/"
    cp $FILE "feeds/export/"
  fi
done

if [ -s log/err-${DATE}.txt ]
then
  # The error file is not-empty.
  echo "E-plikt export failed somehow, below are the error messages"
  echo "-----------------------------------------------------------"
  cat log/err-${DATE}.txt
fi
