#!/bin/bash

# NOTE: Print output is sent in email

DATE=$(date '+%Y%m%d')

if [[ -d "feeds/logs/errors/${DATE}" ]]
then
  rm -r "feeds/logs/errors/${DATE}"
  mkdir "feeds/logs/errors/${DATE}"
else
  mkdir "feeds/logs/errors/${DATE}"
fi

if [[ -d "feeds/logs/errors/${DATE}" ]]
then
  rm -r "feeds/logs/errors/${DATE}"
  mkdir "feeds/logs/errors/${DATE}"
else
  mkdir "feeds/logs/errors/${DATE}"
fi

if [[ -d "feeds/logs/validations/${DATE}" ]]
then
  rm -r "feeds/logs/validations/${DATE}"
  mkdir "feeds/logs/validations/${DATE}"
else
  mkdir "feeds/logs/validations/${DATE}"
fi

node --trace-warnings src/main.js 40000 feeds/export/ > log/log-${DATE}.txt 2> log/err-${DATE}.txt

node --trace-warnings src/main.js 40000 export/publications/all/ > log/log-${DATE}.txt 2> log/err-${DATE}.txt

node --trace-warnings src/main.js 14 export/publications/week/ > log/log-${DATE}.txt 2> log/err-${DATE}.txt

node --trace-warnings src/main.js 1 export/publications/day/ > log/log-${DATE}.txt 2> log/err-${DATE}.txt

