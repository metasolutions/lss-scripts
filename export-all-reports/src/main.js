const { fetchUpdates, publisherOrgNrLookup } = require('./utils');
fs = require('fs');


const timespan = process.argv[2];
const dir = process.argv[3];

/* Set desired timeslot
*  Note: months are zero indexed
*  Current range from last two weeks days
*/
let from = new Date();
from.setDate( from.getDate() - timespan);
const to = new Date();
//to.setDate( to.getDate() - 2  );

console.log( "Looking for updates during the timeslot: " + from.toString() + " - " + to.toString() + '\n');


let toMonth = to.getMonth() + 1;
let toDate = to.getDate();
const toYear = String(to.getFullYear());

if ( toMonth < 10){ 
  toMonth = String(0) + String(toMonth);
}else{
  toMonth = String(toMonth);
}

if ( toDate < 10) { 
  toDate = String(0) + String(toDate);
} else{
  toDate = String(toDate);
}

const stringDate = toYear + toMonth + toDate;

if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}

/*
* Fetches reports published in designated time period. Only pdf format. 
* Generates a rss-feed for each publisher, which can be found in feeds/
*/
fetchUpdates(from, to).then((obj) => {

  const publisherXMLlist = obj['xml'];
  const failedUpdates = obj['failed'];
  for (key of Object.keys(publisherXMLlist)) {

    const county = publisherOrgNrLookup[key]['org-nr'];
    const updatedXMLsize = publisherXMLlist[key].length;
    fs.writeFile(dir +  '/' + county + '.xml', publisherXMLlist[key], function (err) {
      if (err) return console.log(err);
    });
    
  }

  if(
    obj
    && Object.keys(failedUpdates).length === 0
    && Object.getPrototypeOf(failedUpdates) === Object.getPrototypeOf(failedUpdates)
  ){
    console.log('\n' + "No failed updates \n");
  } else {
    let flag  = 1;
    //console.log(Object.keys(failedUpdates).length);
    for (const [key, value] of Object.entries(failedUpdates)) {
      //console.log(value.missing);
      //console.log(key);
      console.error(key + " with title " + value.title + " and item: " + value.item + " and problematic fields: " + value.missing);

      if( value.missing.length !== 0 && value.missing[0] !== 'dcterms:publisher'){
        if( flag ){
          //console.error('Failed updates:');
          flag = 0;
        } 
        // console.error(key + " with problematic fields: " + value.missing);
      }
    }
  }
});

