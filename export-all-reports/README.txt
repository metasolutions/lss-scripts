Generating feeds designed for RSS. Generate new feeds via bash run.sh. Result can be found in export/publications/


Three types of feeds are generated, with varying timespans: day, week, all. 

- Day: entries edited during last 1 day
- Week: entries edited during last 7 days
- All: last edited version of all entries in database

Generated feeds are then available at the followig urls:

https://catalog.lansstyrelsen.se/export/publications/all/
https://catalog.lansstyrelsen.se/export/publications/day/      
https://catalog.lansstyrelsen.se/export/publications/week/

where the organisation number is passed as a route, for instance:

https://catalog.lansstyrelsen.se/export/publications/all/2021002247.xml

Available organisations are:

2021002247 - Stockholm
2021002254 - Uppsala
2021002262 - Södermanland
2021002270 - Östergötland
2021002288 - Jönköping
2021002296 - Kronoberg
2021002304 - Kalmar
2021002312 - Gotland
2021002320 - Blekinge
2021002346 - Skåne
2021002353 - Halland
2021002361 - Västra Götaland
2021002395 - Värmland
2021002403 - Örebro
2021002411 - Västmanland
2021002429 - Dalarna
2021002437 - Gävleborg
2021002445 - Västernorrland
2021002452 - Jämtland
2021002460 - Västerbotten
2021002478 - Norrbotten

