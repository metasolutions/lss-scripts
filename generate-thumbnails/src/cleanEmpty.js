const { EntryStore, EntryStoreUtil, promiseUtil } = require('@entryscape/entrystore-js');
const config = require('../config');
const es = new EntryStore(config.repository);
const esu = new EntryStoreUtil(es);

const doOne = async (obj) => {
  const contextId = obj.context;
  console.log(`\n\nFocusing on länsstyrelse ${obj.name}`);
  const context = es.getContextById(contextId);

  try {
    const emptyImages = [];
    await es.newSolrQuery()
      .context(context)
      .rdfType('http://schema.org/ImageObject')
      .forEach(e => {
        const size = e.getEntryInfo().getSize();
        if (size === undefined || size === 0) {
          emptyImages.push(e);
        }
      });
    console.log(`Found ${emptyImages.length} empty images`);
    // Remove relations to the empty images
    let idx = 0;
    process.stdout.write(`\nRemoving references to empty images:\n`);
    await promiseUtil.forEach(emptyImages, async (imgEntry) => {
      idx += 1;
      process.stdout.write(`\r${idx} of ${emptyImages.length}`);
      const imgEntryURI = imgEntry.getResourceURI();
      const referrers = imgEntry.getReferrers('schema:image');
      const reffererEntries = await esu.loadEntriesByResourceURIs(referrers, context);
      await Promise.all(reffererEntries.map((refEntry) => {
        const md = refEntry.getMetadata();
        md.findAndRemove(refEntry.getResourceURI(), 'schema:image', imgEntryURI);
        return refEntry.commitMetadata();
      }));
    });

    idx = 0;
    process.stdout.write(`\nDeleting empty images:\n`);
    await promiseUtil.forEach(emptyImages, emptyImage => {
      idx += 1;
      process.stdout.write(`\r${idx} of ${emptyImages.length}`);
      return emptyImage.del();
    });
  } catch(e) {
    console.error(`Could not remove empty images for context ${contextId}, moving on to next context`);
  }
  // Clear the cache before we move on to next context.
  es.getCache().clear();
};

const doAll = async () => {
//  const lst = [{name: 'uppsala', context: '33'}];
  const lst = Object.keys(config.lansstyrelser)
    .map( key => ({ name: key, context: config.lansstyrelser[key].context }));
  await promiseUtil.forEach(lst, doOne);
};

es.getAuth().login(config.user, config.password)
  .then(doAll)
  .catch(err => console.error(`Computer says no:`, err));
