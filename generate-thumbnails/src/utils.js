const im = require('imagemagick');
const fs = require('fs');

const getImageEntryId = mainEntryId => `I${mainEntryId}`;

const publicationTypes = [
  "http://data.lansstyrelsen.se/ns/Report",
  "http://data.lansstyrelsen.se/ns/Factsheet",
  "http://data.lansstyrelsen.se/ns/Paper",
  "http://data.lansstyrelsen.se/ns/Newsletter",
  "http://data.lansstyrelsen.se/ns/Brochure",
  "http://data.lansstyrelsen.se/ns/Strategy",
  "http://purl.org/dc/dcmitype/Text",
];

const findPublicationsWithoutImage = async (es, context) => {
  const cache = es.getCache();
  const errors = [];
  const entriesToReturn = [];

  const imageObjects = {};
  const unUsedImageObjects = {};
  const unUsedDigitalDokuments = {};
  await es.newSolrQuery()
    .context(context)
    .rdfType('http://schema.org/ImageObject')
    .forEach(e => {
      unUsedImageObjects[e.getId()] = e;
      imageObjects[e.getResourceURI()] = e;
    });publicationTypes
  await es.newSolrQuery()
    .context(context)
    .rdfType('http://schema.org/DigitalDocument')
    .forEach(e => {
      unUsedDigitalDokuments[e.getResourceURI()] = e;
    });

  const allEntries = es.newSolrQuery()
    .context(context)
    .rdfType(publicationTypes);
  let allEntryCount = 0;
  await allEntries.forEach(async e => {
    allEntryCount += 1;
    const entryMetadata = e.getMetadata();
    const imageResourceUri = entryMetadata.findFirstValue(e.getResourceURI(), 'schema:image');
    if(imageResourceUri){
      const imageEntry = imageObjects[imageResourceUri] ? imageObjects[imageResourceUri] : undefined;
      if (imageEntry) {
        delete unUsedImageObjects[imageEntry.getId()];
      }
      if(!imageEntry?.getEntryInfo().getSize()){
        entriesToReturn.push(e);
      } else {
        // Don't keep the publication in the cache, we won't do anything with it.
        cache.unCache(e);
      }
    } else {
      entriesToReturn.push(e);
    }
    const digitalDocumentUri = entryMetadata.findFirstValue(e.getResourceURI(), 'dcterms:hasVersion');
    if (digitalDocumentUri) {
      delete unUsedDigitalDokuments[digitalDocumentUri];
    }
    entryMetadata.find(e.getResourceURI(), 'dcterms:hasPart').forEach(stmt => {
      delete unUsedDigitalDokuments[stmt.getValue()];
    });

  });
  // Remove all image objets from cache, we won't need them anymore.
  Object.values(imageObjects).forEach(e => cache.unCache(e));
  return [entriesToReturn, allEntryCount, Object.values(unUsedImageObjects), Object.values(unUsedDigitalDokuments)];
}

const getPdfLink = (pub) => pub.getMetadata().findFirstValue(pub.getResourceURI(), 'dcterms:hasVersion');

const getPdfEntry = async (es, pub) => {
  const pdfURI = getPdfLink(pub);
  if (!pdfURI) {
    throw new Error(`Publication has no digital document, URI for entry is: ${pub.getURI()}`);
  }
  console.log(pdfURI);

  const pdfEntryURI = es.getEntryURIFromURI(pdfURI);
  const pdfEntry = await es.getEntry(pdfEntryURI);
  return pdfEntry;
}

const downloadPdf = async (pub, pdfEntry) => {
  const size = pdfEntry.getEntryInfo().getSize();
  if (size === undefined || size === 0) {
    throw new Error(`Digital document has no resource, URI for digital document is: ${pdfEntry.getEntryURI()}`)
  }
  const localPdfFilename = `downloads/${pub.getId()}.pdf`;
  const pdfResource = pdfEntry.getResource(true);
  const writeStream = fs.createWriteStream(localPdfFilename);
  await pdfResource.get(writeStream).catch(err => {
    console.error('knas:', err);
    throw err;
  });
  // MP: I do not understand why we need to wait, it seems if we don't the file is not written to disc
  // before we read it and start to convert it.
  await new Promise(suc => setTimeout(suc, 3000));
  return true;
}

const thumbnailFirstPage = async (fromFilename, toFilename) => {
  const firstPage = `${fromFilename}[0]`; // Add the [0] after the file name to force only thubmnailing the first page
  await new Promise((onsuccess, onerror) => {
    im.convert(['-thumbnail', '450x', '-background', 'white', '-alpha', 'remove', firstPage, toFilename],
      (err, stdout) => {
        if (err) onerror('File is not a pdf');
        else onsuccess(true);
      });
  });
  // MP: Again I do not understand why we need to wait, it seems if we don't the file is not written to disc
  // before we can upload it.
  await new Promise(suc => setTimeout(suc, 3000));
  fs.unlinkSync(fromFilename);
};

const thumbnailPdf = (pub) => {
  const localPdfFilename = `downloads/${pub.getId()}.pdf`;
  const thumbnailFilename = `downloads/${getImageEntryId(pub.getId())}.png`;
  return thumbnailFirstPage(localPdfFilename, thumbnailFilename)
};

const createAndCommitThumbnailEntry = (context, mainEntryId) => {
  const thumbnailEntryId = getImageEntryId(mainEntryId);
  const es = context.getEntryStore();
  const euri = es.getEntryURI(context.getId(), thumbnailEntryId);

  // First try to load existing thumbnailEntry
  return es.getEntry(euri)
    // If it does not exist, create it.
    .catch((err) => context.newEntry(thumbnailEntryId)
      .add('rdf:type', 'http://schema.org/ImageObject')
      .addL('dcterms:title', thumbnailEntryId).commit())
};

const uploadPngToThumbnailEntry = (pub, thumbnailEntry) => {
  if (thumbnailEntry.getEntryInfo()?.getSize()) return Promise.reject(`File already uploaded for ${thumbnailEntry.getId()}`);
  const thumbnailFilename = `downloads/${getImageEntryId(pub.getId())}.png`;
  if (!fs.existsSync(thumbnailFilename)) return Promise.reject(`File ${thumbnailFilename} not found`)

  const mimetype = 'image/png';
  console.log(`uploading ${thumbnailFilename} to ${thumbnailEntry.getId()}`);
  const file = fs.createReadStream(thumbnailFilename);
  const resource = thumbnailEntry.getResource(true)
  console.log('res uri:', resource.getResourceURI());
  return resource.putFile(file, mimetype).catch(err => {
    console.log(`failed to put file ${thumbnailFilename} to entry ${thumbnailEntry.getId()}:`, err);
    throw err;
  }).finally(() => {
    fs.unlinkSync(thumbnailFilename);
  });
}

const addImagePropertyToPublicationEntry = (publicationEntry, imageEntryUri) => {
  publicationEntry.add('schema:image', imageEntryUri);
  return publicationEntry.commitMetadata();
}

module.exports = {
  findPublicationsWithoutImage,
  getPdfLink,
  getPdfEntry,
  downloadPdf,
  thumbnailPdf,
  createAndCommitThumbnailEntry,
  uploadPngToThumbnailEntry,
  addImagePropertyToPublicationEntry
}
