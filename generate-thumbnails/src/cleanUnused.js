const { EntryStore, EntryStoreUtil, promiseUtil } = require('@entryscape/entrystore-js');
const config = require('../config');
const es = new EntryStore(config.repository);

const publicationTypes = [
  "http://data.lansstyrelsen.se/ns/Report",
  "http://data.lansstyrelsen.se/ns/Factsheet",
  "http://data.lansstyrelsen.se/ns/Paper",
  "http://data.lansstyrelsen.se/ns/Newsletter",
  "http://data.lansstyrelsen.se/ns/Brochure",
  "http://data.lansstyrelsen.se/ns/Strategy",
  "http://purl.org/dc/dcmitype/Text",
];

const doOne = async (obj) => {
  const contextId = obj.context;
  console.log(`\n\nFocusing on länsstyrelse ${obj.name}`);
  const context = es.getContextById(contextId);

  try {
    const uri2image = {};
    await es.newSolrQuery()
      .context(context)
      .rdfType('http://schema.org/ImageObject')
      .forEach(e => {
        uri2image[e.getResourceURI()] = e;
      });
    await es.newSolrQuery()
      .context(context)
      .rdfType(publicationTypes)
      .forEach((e, idx) => {
      process.stdout.write(`\rIndexing publication nr ${idx}`);
        e.getMetadata().find(e.getResourceURI(), 'schema:image').forEach(stmt => {
          delete uri2image[stmt.getValue()];
        });
      });
    const unUsedImages = Object.values(uri2image);

    console.log(`\n\nFound ${unUsedImages.length} unused images`);
    // Clear the cache before we move on to next context.
    let idx = 0;
    process.stdout.write(`\nRemoving unused images:\n`);
    await promiseUtil.forEach(unUsedImages, unused => {
      idx += 1;
      process.stdout.write(`\r${idx} of ${unUsedImages.length}`);
      return unused.del();
    });
  } catch(e) {
    console.error(`Could not remove unused images for context ${contextId}, moving on to next context`);
  }
  es.getCache().clear();
};

const doAll = async () => {
  const lst = Object.keys(config.lansstyrelser)
    .map( key => ({ name: key, context: config.lansstyrelser[key].context }));
  await promiseUtil.forEach(lst, doOne);
};

es.getAuth().login(config.user, config.password)
  .then(doAll)
  .catch(err => console.error(`Computer says no:`, err));
