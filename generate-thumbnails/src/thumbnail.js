const fs = require('fs');
const { EntryStore, promiseUtil } = require('@entryscape/entrystore-js');
const {
  findPublicationsWithoutImage,
  getPdfLink,
  getPdfEntry,
  downloadPdf, 
  thumbnailPdf, 
  createAndCommitThumbnailEntry, 
  uploadPngToThumbnailEntry, 
  addImagePropertyToPublicationEntry
} = require("./utils.js");
const config = require('../config');
const es = new EntryStore(config.repository);

const report = {
  noPDFLink: [],
  brokenPDFLink: [],
  emptyPDF: [],
  notAPDF: [],
  thumbnailed: [],
  thumbnailedCount: 0,
  checkedCount: 0,
  unUsedImageObjects: [],
  unUsedDigialDocuments: []
};

const context2Lst = {};
Object.keys(config.lansstyrelser).forEach(key => context2Lst[config.lansstyrelser[key].context] = key);

const reportEntry = (entry) => {
  const title = entry.getMetadata().findFirstValue(entry.getResourceURI(), 'dcterms:title') || '';
  return `${context2Lst[entry.getContext().getId()]}: ${entry.getId()} - ${title}`;
};

const thumbnailPublication = async (pub) => {
  const publicationEntryId = pub.getId();
  let pdfEntry;
  let pdfResourceExists;
  try {
    console.log(`Getting publication entry ${publicationEntryId}`);
    pdfEntry = await getPdfEntry(es, pub);

    console.log(`Downloading the pdf ${publicationEntryId}`);
    await downloadPdf(pub, pdfEntry);

    console.log(`Thumbnailing ${publicationEntryId}`);
    await thumbnailPdf(pub);

    console.log(`Creating entry for ${publicationEntryId}`);
    const thumbnailEntry = await createAndCommitThumbnailEntry(pub.getContext(), publicationEntryId);

    console.log(`Uploading thumbnail for ${publicationEntryId}`);
    await uploadPngToThumbnailEntry(pub, thumbnailEntry);

    console.log(`Connect entry: ${thumbnailEntry.getResourceURI()} to ${publicationEntryId}`);
    await addImagePropertyToPublicationEntry(pub, thumbnailEntry.getResourceURI());

    report.thumbnailed.push(reportEntry(pub));
    report.thumbnailedCount += 1;
    console.log(`---- Done with publication ${publicationEntryId} ----`);
  } catch(e) {
    console.log(`Aborted thumbnailing publication with id ${publicationEntryId}`);
    console.log(e.message || e);

    if (!getPdfLink(pub)) {
      report.noPDFLink.push(reportEntry(pub));
    } else if (pdfEntry) {
      const size = pdfEntry.getEntryInfo().getSize();
      if (size === undefined || size === 0) {
        report.emptyPDF.push(reportEntry(pub));
      } else {
        report.notAPDF.push(reportEntry(pub));
      }
    } else {
      report.brokenPDFLink.push(reportEntry(pub));
    }
  }
};

const doOne = async (obj) => {
  const contextId = obj.context;
  console.log(`Focusing on länsstyrelse ${obj.name}`);
  const context = es.getContextById(contextId);
  try {
    let [publicationsWithoutImage,
      checkedPublicationCount,
      unUsedImageObjects,
      unUsedDigitalDocuments,
    ] = await findPublicationsWithoutImage(es, context);
    unUsedImageObjects.forEach(e => report.unUsedImageObjects.push(reportEntry(e)));
    unUsedDigitalDocuments.forEach(e => report.unUsedDigialDocuments.push(reportEntry(e)));
    report.checkedCount += checkedPublicationCount;
    console.log(`Found ${publicationsWithoutImage?.length} publications without image in context ${contextId}`);
//    publicationsWithoutImage = publicationsWithoutImage.slice(0, 3);

    // Do all the publications
    await promiseUtil.forEach(publicationsWithoutImage, thumbnailPublication);
  } catch(e) {
    console.error(`Could not get publications for context ${contextId}, moving on to next context`);
  }

  // Clear the cache before we move on to next context.
  es.getCache().clear();
};

const doAll = async () => {
//  const lst = [{name: 'uppsala', context: '33'}];
  const lst = Object.keys(config.lansstyrelser)
    .map( key => ({ name: key, context: config.lansstyrelser[key].context }));
  await promiseUtil.forEach(lst, doOne);
};

const writeReport = () => {
  fs.writeFileSync('report.json', JSON.stringify(report, null, '  '));
};

es.getAuth().login(config.user, config.password)
  .then(doAll)
  .then(writeReport)
  .catch(err => console.error(`Computer says no:`, err));
