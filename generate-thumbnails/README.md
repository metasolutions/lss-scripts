# Generates thumbnails for reports
The process goes through every länsstyrelse and does the following:

1. Finds all imageObjects already created (PNGs) and checks for their size
2. Finds all publications and checks if there is a PNG moves on to the next
3. If there is no PNG it does the following:
4. Downloads the PDF
5. Creates a PNG of the first page via imagemagick
6. Uploads the PNG to EntryStore
7. Updated the publication with a link to the PNG

The process can go wrong in two ways:

1. There is no PDF for the publication
2. The file associated with the publication is not a PDF, typically it is a DOCX file.

After finishing a report.json is created containing information on:

1. how many publications where checked
2. how many thumbnails that was created
3. list of publications for which thumbnails where created
4. list of publications for which links to PDFs are missing (no link)
5. list of publications for which links to PDFs are broken (no entry)
6. list of publications for which the links leads to an empty PDF (entry but no resource)
7. list of publications for which something else than a PDF was found (entry with non-pdf resource)
8. list of unused images
9. list of unused digital documents (typically PDFs)

## Installation / Configuration
Run `yarn` in the generate-thumbnails folder.

Copy over config.js_example to config.js and adapt it.
Make sure there is a download folder inside of generate-thumbnails.

### ImageMagic
Make sure you have imagemagick and ghostscript installed on your system.

By default ImageMagic does not allow generating images from PDF.
You first have to allow it in the file `/etc/ImageMagick-6/policy.xml`.
Make sure the follwing i stated somewhere:

    <policy domain="module" rights="read|write" pattern="{PS,PDF,XPS}" />
