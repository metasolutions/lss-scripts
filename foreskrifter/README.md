# Extrahera metadata från länsstyrelsens föreskrifter i pdf-format

## Hämta pdf:er från webben

Om pdf:erna har förutsägbara url:er, som i fallet Västra Götaland, går det att hämta filer med `wget` i en `for`-loop. Du måste då veta hur många dokument som finns per år. Exempel: 2020 finns det 27 dokument för Västra Götaland:

`for i in {01..27}; do wget https://ext-dok.lansstyrelsen.se/forfattningar/Filer/o/14FS_2020_$i.pdf; done`

För övriga länsstyrelser, måste vi få pdf:erna zippade eller tar.gz:ade.

## Konvertera till txt

`for file in ./*.pdf; do pdftotext $file "$file.txt"; done`

Ett av dokumenten är en förteckning över gällande författningar. Ta bort det dokumentet (txt och pdf) innan du fortsätter. Hitta det med `grep -iR förteckning ` (i: case insensitive, R: recursive).

## Kör skriptet. 

`convert.sh` kör respektive awk-skript på alla textfiler och genererar en csv, som öppnas i Libreoffice.

