# Sätt output record separator till tab, dvs skriv ut på samma rad med tabbar emellan
BEGIN { 
  concat_title_mode = 0 
}

# År (issued)
# löpnummer (identifier)
NR==3 {
  issued = sprint substr($2, 0, 4);
  identifier = $0;
}

# rubriken är på flera rader, från rad 8 och fram ned till raden som börjar med "beslutat den"
# vi sätter en flagga som säger att vi håller på att "samla in" titeln
NR==7 {
  concat_title_mode = 1;
}

# Bemyndigande
# här slutar rubriken
/med stöd av/ {
  { s=$0; getline }{ s = s " " $0; getline }{ s = s " " $0 }
  s = substr(s, match(s, /[1-9]/));
  afterkap = substr(s, 7);
  stop = match(afterkap, /[\.)\,]/) + 7;
  based_on = substr(s, 0, stop);
}

# Beslutsdatum
/^besluta(t|de) den [0-9]{1,2}\ \w{3,}\ 20[0-2][0-9]\./ { 
  concat_title_mode = 0;
  date = sprintf("%s %s %s",$3 ,$4 ,substr($5, 0, length($5)-1));
}

# Rubrik (title)
concat_title_mode == 1 {
  title = title $0; # slå ihop strängar mellan tryckdatum och beslutsdatum
}

# Ikraftträdande
/i\ kraft/ { 
  s = $0;
  lastchar = substr($0, length($0), length(1));
  if (lastchar  != "."){
    getline;
    s = s " " $0;
  }
  match(s, /[0-3]?[0-9]\ \w{3,} (19|20)[0-9]{1,2}/); 
  inforce = substr(s, RSTART, RLENGTH);
}

# Kommun
 /kommun(er)?/ { kommuner = sprintf( "%s",$0"\t" )}
# /\skommun(er)?\s/ 

END { 
  ORS="\t"
  print identifier;
  print issued;
  print date;
  print inforce;
  print based_on;
  ORS="\n";
  print title;
}
