
# convert to txt
for file in pdf/*.pdf; do
  test -f "$file.txt" || pdftotext $file "$file.txt"; 
done

# create csv headers
echo -e `cat headers.csv` > temp.csv

# awk through files
for file in pdf/14FS_*.txt; do
  if [[ $file =~ '2021' ]]; then
    awk -f 2021-to-csv.awk $file;
  else
    awk -f 2020-to-csv.awk $file;
  fi
done >> temp.csv

# open in libreoffice
libreoffice temp.csv
