const config = require('./config'); // the config file includes values for repo, context, username et.c.
const { EntryStore, EntryStoreUtil, promiseUtil, namespaces } = require('entrystore-js');
const LogModule = require('./log'); // for logging events
const log = LogModule('file-test');

const es = new EntryStore(config.repository);
const fs = require('fs');

// const downloadFile = async (url, filename) => {
//   const axios = require('axios');
// 
//   const file = filesystem.createWriteStream(filename); //TODO kolla om det finns andra filformat, kolla filändelse
//   axios({
//     url: url,
//     method: "GET",
//     responseType: 'stream'
//   }).then(response => {
//     response.data.pipe(file);
//     return Promise.resolve("Go getter-attityd!");
//   }).catch(err => {
//     log.error("jävel och slaggis")
//     return Promise.reject(err);
//   })
// }

// here it all starts:

log.info(`logging in ${config.user} to context ${config.context}:`);
es.getAuth().login(config.user, config.password).then( async () => {
  const filename = './test.pdf';
  const entryURI = 'https://catalog.lansstyrelsen.se/store/19/entry/_W_2021';
  const entry = await es.getEntry(entryURI)
    .catch(err => log.error(err));

  const resource = await entry.getResource();
  resource.putFile(fs.createReadStream(filename), 'application/pdf')
    .then(() => log.info(`Uploaded ${filename} to ${resource.getResourceURI()}`))
    .catch(err => {log.error(`failed to put ${filename} to ${resource.getResourceURI()}`); log.error(err)});
}).catch((err)=> console.log(`Computer says no:\n ${err}`, err));

