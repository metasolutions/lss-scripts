createEntry = (context, publication, subjectUris) => {
  const newEntry = context.newEntry(publication.id)
    .add('rdf:type', publication.rdfType)
    .addL('dcterms:title', publication.title)
    .add('dcterms:publisher', publication.publisher)
    .addL('dcterms:identifier', publication.identifier)
    .addD('dcterms:issued', publication.year, 'xsd:gYear')
    .addL('dcterms:abstract', publication.summary)
    .addL('dcterms:subject', publication.keyword)

  if(publication.issnUri) newEntry.add('schema:issn', publication.issnUri);
  if(publication.isbn) newEntry.addL('schema:isbn', publication.isbn);

  if(publication.documentId) {
    const documentUrl  = `${context._resourceURI}/resource/${publication.documentId}`;
    console.log(`${publication.identifier}: adding hasVersion ${documentUrl}`);
    newEntry.add('dcterms:hasVersion', documentUrl);
  }
  if(publication.appendices.length) {
    publication.appendices.forEach(apx => {
      const documentUrl = `${context._resourceURI}/resource/${apx.identifier}`;
      console.log(`${publication.identifier}: adding hasPart ${documentUrl}`);
      newEntry.add('dcterms:hasPart', documentUrl);
    });
  };
  if(publication.journalnumber != "")
    newEntry.addL('adms:identifier', publication.journalnumber);
  publication.subject?.forEach(s => addSubjectsToEntry(subjectUris, newEntry, s));

  return newEntry;
}

const addSubjectsToEntry = (subjectsUris, newEntry, subject) => {
  const uri = subjectsUris[subject.trim().toLowerCase()];
  if (uri) {
    newEntry.add('dcterms:subject', uri);
  } else if (subject.indexOf('&') != -1 || subject.indexOf('＆') != -1) {
    subject.split(/[＆&]/).forEach(part => {
      const partUri = subjectsUris[part.trim().toLowerCase()];
      if (partUri) {
        newEntry.add('dcterms:subject', partUri);
      }
    });
  } else if (subject.indexOf('och') != -1) {
    subject.split('och').forEach(part => {
      const partUri = subjectsUris[part.trim().toLowerCase()];
      if (partUri) {
        newEntry.add('dcterms:subject', partUri);
      }
    });
  }
}

const setPublisher = (pubs, lstLetter) => {
  pubs.forEach(pub => pub.publisher = `http://data.lansstyrelsen.se/ns/county/${lstLetter}`);
  return pubs;
}

const getRdfTypeUri = pub => {
  if(!pub.type || pub.type === 'other') {
    rdfTypeUri = 'http://purl.org/dc/dcmitype/Text';
  } else 
    rdfTypeUri = 'http://data.lansstyrelsen.se/ns/' + pub.type.charAt(0).toUpperCase() + pub.type.slice(1);
  return rdfTypeUri;
}

const addRdfTypes = pubs => {
  pubs.forEach(p => p.rdfType = getRdfTypeUri(p));
  return pubs;
}

const getSubjectUris = async (es, subjectContextId) => {
  const subjectIdx = {};
  const subjContext = es.getContextById(subjectContextId);
  await es.newSolrQuery()
   .context(subjContext)
   .rdfType('skos:Concept')
   .forEach((conceptEntry) => {
     const resourceUri = conceptEntry.getResourceURI();
     subjectIdx[conceptEntry.getMetadata()
       .findFirstValue(resourceUri, 'skos:prefLabel')
       .toLowerCase()
     ] = resourceUri;
  });
  return subjectIdx;
}

const createEntryAndCommit = (context, pub, subjectUris) =>  {
  const newEntry = createEntry(context, pub, subjectUris);
  return newEntry.commitMetadata()
    .catch(err =>  {
      if(err.status == 404) {
        // will first try to update metadata on an existing entry,
        // and if that fails, and it's because the entry not yet exists, create the entry
        return newEntry.commit()
          .catch(err => {
            if(err.response && err.response.res) console.error(`On commitMetadata: ${err.response?.res?.statusCode} ${err.response?.res?.statusMessage}: ${err.response?.res?.text}`);
            else console.error('On commitMetadata: ', err);
          });
      } else return Promise.reject(err);
    });
}


module.exports = {
  setPublisher,
  getSubjectUris,
  addRdfTypes,
  createEntryAndCommit,
}
