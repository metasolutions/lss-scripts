Array.prototype.uniq = function() {
  seen = [];
  this.forEach(el => { seen.includes(el) || seen.push(el) });
  return seen;
}

Array.prototype.dubletter = function() {
  seen = {}
  this.forEach(el => {
    if(Object.keys(seen).includes(el)){
      seen[el] += 1;
    } else {
      seen[el] = 1;
    }
  });
  return Object.keys(seen).filter(key => seen[key] > 1);//seen[key] > 2);
}

const conformId = str => str && str.replaceAll(/[\.-]/ig, ':');
const isValidId = str => str && conformId(str).match(/^\d{4}:\d+$/);

const replaceIdentifierForPostsWithBadIdentifier = publications => {
  const years = publications.map(p => p.year).uniq().sort();
  years.forEach(year => {
    let nr = 0;
    pubsOfYear = publications.filter(p => p.year === year);
    pubsOfYear
      .filter(pub => !isValidId(pub.identifier))
      .forEach(pub => {
        pub.identifier = `${year}:_${++nr}`;
      });
  });
  return publications;
}

const resolveDuplicateIdentifiers = publications => {
  const years = publications.map(p => p.year).uniq().sort();
  years.forEach(year => {
    pubsOfYear = publications.filter(p => p.year === year);
    dublicateIds = pubsOfYear.map(p => p.identifier).dubletter();
    dublicateIds.forEach(id => {
      pubsWithSameId = publications.filter(p => p.identifier === id);
      let nr = 0;
      pubsWithSameId.forEach(pub => {
        pub.identifier = `${pub.identifier}_${++nr}`;
      });
    });
  });
  return publications;
}

const addLstLetterToIdentifier = (pubs, lstLetter) => {
  pubs.forEach(p => p.identifier = `${lstLetter}:${p.identifier}`);
  return pubs;
}

const cleanIdentifiers = pubs => {
  pubs.forEach(p => p.identifier = conformId(p.identifier));
  return pubs;
}

const setIdentifierAsEntryId = pubs => {
  pubs.forEach(p => p.id = p.identifier.replaceAll(':', '_'));
  return pubs;
}

const setIssnUriOrIsbn = pubs => {
  pubs.forEach(pub => {
    const baseUri = 'http://issn.org/resource/ISSN/';

    const issnRegex = /^(ISSN\s?)?\d{4}-\d{3}[\dxX]$/;
    const issnMatch = pub.issn?.match(issnRegex);
    
    const isbnRegex = /^(ISBN\s?)?\d[\d-]*\d/;
    const isbnMatch = pub.issn?.match(isbnRegex);
    
    if(issnMatch) {
      const onlyIssnRegex = /\d{4}-\d{3}[\dxX]/;
      const issn = issnMatch[0].match(onlyIssnRegex);
      pub.issnUri = `${baseUri}${issn}`;
    } else if(isbnMatch) {
      const onlyIsbnRegex = /\d[\d-]*\d/;
      isbn = isbnMatch[0].match(onlyIsbnRegex);
      pub.isbn = `${isbn}`;
    }
  });
  return pubs;
}


module.exports = {
  addLstLetterToIdentifier,
  setIdentifierAsEntryId,
  resolveDuplicateIdentifiers,
  replaceIdentifierForPostsWithBadIdentifier,
  setIssnUriOrIsbn,
  cleanIdentifiers,
}


