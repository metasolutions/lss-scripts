
const fs = require('fs'); // file system utils from Node.js

const dateStr = () => {
  const date = new Date(Date.now());
  return date.toISOString(date).substring(0,16).replace(/[:|T]/g,'-');
}

const createSitevisionLog = pub => {
  const logpost = {
    svid: pub.svid,
    svurl: pub.svurl,
    id: pub.id,
    identifier: pub.identifier,
    documentId: pub.documentId,
    appendices: pub.appendices,
  }
  return logpost;
}

const createIssnLog = p => {
  return {
    identifier: p.identifier, 
    originalIssn: p.issn,
    issnUri: p.issnUri,
    isbn: p.isbn
  }
}

const saveLog = (log, filename) => {
  filename = `./log/${filename}.json`;
  fs.writeFileSync(filename, JSON.stringify(log, null, 2));
}

const getNow = () => {
  let now = new Date(Date.now());
  now = now.toISOString().substring(0,19);
  now = now.replace(':', '');
  now = now.replace(':', '');
  now = now.replace('T', '_');
  return now;
}

function LogModule(name, logNoise){
  const filename = `log/${name}-${dateStr()}.log`;  

  const log = (level, text) => {
    if(typeof(text) === "object"){
//      temp = "\n";
//      for(grej in text) temp += `\t${grej}: ${text[grej]}\n`;
//      text = temp;
      text = JSON.stringify(text, null, "  ");
    }
    if(level !== 'debug' || logNoise === 'debug'){
      const logMessage = `[${level}] ${text}`;
      fs.appendFile(filename, `${logMessage}\n`, (err) => {
        if(err) {
          console.error(`Could not write to logfile!: ${err}`);
        }
      });

      switch(logNoise){
        case "silent":
          break;
        case "error":
          if(level in ['error', 'idConflict', 'badIdFormat']) 
            console.error(logMessage);
          break;
        case "noisy":
          if(level === 'debug') console.debug(logMessage);
        default:
          if(level in ['info', 'noId']) console.log(logMessage)
          else 
            console.error(logMessage);
      }
    };
  }
      
  return {
    debug: text => log('debug', text),
    info: text => log('info', text),
    error: text => log('error', text),
    idConflict: text => log('idConflict', text),
    noId: text => log('noId', text),
    badIdFormat: text => log('badIdFormat', text),
    missingFile: text => log('missingFile', text),
    pngFailure: text => log('pngFailure', text),
    entryFailure: text => log('entryFailure', text),
    uploadError: text => log('uploadError', text)
  }
}

module.exports = {
  LogModule,
  createSitevisionLog,
  createIssnLog,
  getNow,
  saveLog
}
