const config = require('./config'); // the config file includes values for repo, context, username et.c.
const { EntryStore, promiseUtil, namespaces } = require('entrystore-js');
const { setIssnUriOrIsbn, cleanIdentifiers, addLstLetterToIdentifier, setIdentifierAsEntryId, resolveDuplicateIdentifiers, replaceIdentifierForPostsWithBadIdentifier } = require('./identifier');
const { createEntryAndCommit, setPublisher, addRdfTypes, getSubjectUris } = require('./publication');
const { setDocumentId, correctUrls, getExtension, downloadAndUploadAppendices, downloadAndUploadMainDocument } = require('./digitaldocument');
const log = require('./log');
const es = new EntryStore(config.repository);
const dryrun = process.argv.some(a => a === '--dryrun');

namespaces.add('adms', 'http://www.w3.org/ns/adms#');
namespaces.add('schema', 'http://schema.org/');
namespaces.add('lss', 'http://data.lansstyrelsen.se/ns/');

const lstarg = process.argv[2];
const lst = config.lansstyrelser[lstarg];
if(!lst){
  console.error(`${lstarg} not found`);
  return 1;
}

const now = log.getNow();
console.log(`\nimport run at ${now}`);
console.error(`\nimport run at ${now}`);
const importlog = { 
  lansstyrelse: lstarg,
  contextId: lst.context,
  importedAt: now,
  publications: [],
};
const issnLog = [];

let publications = require(lst.inputfile);
// publications = publications.slice(0,30);
// publications = publications.filter(pub => pub.appendix.length);
console.log(`found ${publications.length} publications in ${lst.inputfile}`);

// set values
publications = addRdfTypes(publications);
publications = setPublisher(publications, lst.kod);
publications = replaceIdentifierForPostsWithBadIdentifier(publications);
publications = resolveDuplicateIdentifiers(publications);
publications = cleanIdentifiers(publications);
publications = addLstLetterToIdentifier(publications, lst.kod);
publications = setIdentifierAsEntryId(publications);
publications = correctUrls(publications);
publications = setDocumentId(publications);
publications = setIssnUriOrIsbn(publications);

publications.forEach(p => issnLog.push(log.createIssnLog(p)));

if(dryrun) {
  publications.forEach(p => importlog.publications.push(log.createSitevisionLog(p)));
  log.saveLog(importlog, `${lstarg}_${now}`);
  log.saveLog(issnLog, `${lstarg}_issn_${now}`);
  console.log('dryrun, exiting');
  return 0;
}



console.log(`logging in ${config.user} to context ${lst.context}:`);
es.getAuth().login(config.user, config.password).then( async () => {
  const context = es.getContextById(lst.context);
  const subjectUris = await getSubjectUris(es, config.subjectContext)
    .catch(err => console.error(err));

  await promiseUtil.forEach(publications, async pub => {
    await createEntryAndCommit(context, pub, subjectUris)
      .catch(err => console.error('error on creating entry: ', err))
    const documentResourceUri = await downloadAndUploadMainDocument(es, context, pub)
      .catch(err => console.error('error on main document upload:', err))
    importlog.publications.push(log.createSitevisionLog(pub));

    if(pub.appendix?.length){
      console.log(`${pub.appendix.length} appedices to download:`);
      await downloadAndUploadAppendices(es, context, pub)
        .catch(err => console.error('error on appendix document upload:', err))
    }
  }).catch(err => console.error(err.status ? `promiseUtil catch: ${err.status}:`:err));

  log.saveLog(importlog, `${lstarg}_${now}`);
  log.saveLog(issnLog, `${lstarg}_issn_${now}`);
}).catch(err => console.error(`Computer says no: ${err}`));

