# todo

- [x] problem med issn/isbn - skapas men kommer inte in/syns inte
- [ ] png
- [x] kolla så gamla poster uppdateras, inte dupliceras.
- [x] fixa loggning
- [x] strukturera koden väl
- [ ] lägg in i lss-skript-repot
- [x] stöd för annat än pdf
- [x] appendix
- [x] skapa sandlåda för två länsstyrelser
- [x] fixa behörighet för mia och marcus
- [x] maila och bjud in


## loggning

Vad ska loggas?
- [x] svid, svurl, resursurl, id för varje publikation
- [x] hur isbn/issn har parsats
- [x] errors
- [ ] när en entry inte lyckas skapas
- [ ] när ett dokument inte lyckas laddas uppdateras
- [ ] när ett dokument inte heter .pdf men laddas upp som pdf
- [ ] när en publikation innehåller felaktig
