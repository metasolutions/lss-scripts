const { promiseUtil } = require('entrystore-js');

const setDocumentId = pubs => {
  pubs.forEach(p => {
    p.documentId = p.file?.length && `D${p.id}`;
    p.appendices = [];
    let ix = 0;
    p.appendix.forEach(apx => {
      ix = ix + 1;
      p.appendices.push( {
        originUrl: apx,
        identifier: `D${p.id}_part_${ix}`,
        extension: getExtension(apx),
      });
    });
  });
  return pubs;
}

const downloadFile = async (url, filename) => {
  const axios = require('axios');
  const filesystem = require('fs');
  const file = filesystem.createWriteStream(filename);
  axios({
    url: url,
    method: "GET",
    responseType: 'stream'
  }).then(response => {
    response.data.pipe(file);
    return Promise.resolve();
  }).catch(err => {
    console.error('Error in axios GET');
    return Promise.reject(err);
  })
}

const correctUrls = pubs => {
  pubs.forEach(pub => {
    pub.file = pub.file?.replace('https://lansstyrelsen.sehttps', 'https');
    pub.appendix = pub.appendix.map(apx => apx = apx.replace('https://lansstyrelsen.sehttps', 'https'));
  });
  return pubs;
}

const getContextId = context => context._resourceURI.split('/').pop();

const downloadAndUploadAppendices = async (es, context, pub) => {
  if(!pub.appendix?.length) return Promise.reject(`${pub.id} (${pub.svid}) has no appedices`);
  promiseUtil.forEach(pub.appendices, async apx => {
    let filename = `./download/${apx.identifier}`;
    if(apx.extension.length) filename = `${filename}.${apx.extension}`;
    return downloadAndUploadFile(es, context, apx.originUrl, filename, apx.identifier)
      .catch(err => Promise.reject(err));
  })
}

const downloadAndUploadMainDocument = async (es, context, pub) => {
  if(!pub.file) return Promise.reject(`${pub.id} (${pub.svid}) has no file`);
  const identifier = `D${pub.id}`;
  const extension = getExtension(pub.file);
  const filename = `./download/${identifier}.${extension}`;
  return downloadAndUploadFile(es, context, pub.file, filename, identifier)
    .catch(err => Promise.reject(err));
}

const getExtension = filename => {
  if(!filename) return '';
  const validExtensions = ['xlsx', 'pptx', 'docx', 'html', 'zip', 'jpeg', 'pdf'];
  let extension = filename.split('/').pop();
  extension = filename.split('.').pop().toLowerCase();
  if(extension === 'jpg') extension = 'jpeg';
  if(validExtensions.includes(extension)) return extension;
  return '';
}

const getMimetype = extension => {
  extension = extension.toLowerCase();
  if(extension === 'xlsx') return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  if(extension === 'pptx') return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
  if(extension === 'docx') return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
  if(extension === 'zip') return 'application/zip';
  if(extension === 'html') return 'text/html';
  if(['jpg', 'jpeg'].includes(extension)) return 'image/jpeg';
  return 'application/pdf';
}

const downloadAndUploadFile = async (es, context, url, filename, publicationId) => {
  const fs = require('fs');
  if(fs.existsSync(filename))
    console.log(`${publicationId} already downloaded. continuing to upload`)
  else {
    const file = await downloadFile(url, filename)
      .catch(err => Promise.reject(`failed to download ${url} to ${filename}`));
    console.log(`downloaded ${url} to ${filename}`);
  }

  const extension = getExtension(filename);

  const documentEntry = await getOrCreateDocumentEntryAndCommit(es, context, publicationId, extension)
    .catch(err => Promise.reject(`failed to get or create document entry for ${publicationId}: ${err}`));
  if(!documentEntry) return Promise.reject(`documentEntry for ${publicationId} was empty`);

  await addLabelToEntryInfoGraph(documentEntry, extension)
    .catch(err => console.error('error on addLabel:', err));

  const mimetype = getMimetype(extension);
  if(mimetype !== 'application/pdf') console.log(`${publicationId}: Setting mimetype to ${mimetype}`);

  return uploadFile(documentEntry, filename, mimetype)
    .catch(err => {
      if(typeof(err) === 'string' && err.startsWith('File already uploaded')){
        console.log(`File for entry ${publicationId} already uploaded :)`);
        return Promise.resolve(err);
      } else Promise.reject(`error on uploading ${filename} to entry ${documentEntry.getId()}: ${err}`);
    });
}

const getOrCreateDocumentEntryAndCommit = async (es, context, id) => {
  // first, try to create entry. if it fails, see if it exists
  return context.newEntry(id)
    .addL('dcterms:title', id)
    .add('rdf:type', 'http://schema.org/DigitalDocument')
    .commit()
    .catch(err => {
      if(err.status == 409) { // 409 Conflict. Entry exists.
        console.log(`entry for ${id} exists. getting it`)
        return getEntry(es, context, id);
      } else {
        return Promise.reject(`could not create entry for ${id}: ${err.status}`);
      }
    });
}

const addLabelToEntryInfoGraph = async (entry, extension) => {
  const entryInfo = entry.getEntryInfo();
  const entryInfoGraph = entryInfo.getGraph();
  // entryInfoGraph.addL('rdfs:label', `koko.pptx`)
  entryInfoGraph.addL(entry.getResourceURI(), 'rdfs:label', `${entry.getId()}.${extension}`)
  return entryInfo.commit()
    .catch(err => Promise.reject(`could not commit entryInfo for ${entry.getId()} after adding info to graph`));
}

const uploadFile = async (entry, filename, mimetype) => {
  if(entry.getEntryInfo().getSize()) return Promise.reject(`File already uploaded for ${entry.getId()}`);
  const fs = require('fs');
  const file = fs.createReadStream(filename);
  return entry.getResource(true).putFile(file, mimetype)
    .catch(err => `failed to put file ${filename} to entry ${entry.getId()}`);
}

const getEntry = (es, context, id) => {
  euri = es.getEntryURI(getContextId(context), id);
  return es.getEntry(euri)
    .catch(err => Promise.reject(`could not get existing entry ${id}, ${euri}`));
}

module.exports = {
  downloadAndUploadMainDocument,
  downloadAndUploadAppendices,
  getMimetype,
  correctUrls,
  setDocumentId,
  getExtension,
  uploadFile, 
  downloadFile
}

