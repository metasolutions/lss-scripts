const config = require('./config');
const { EntryStore, EntryStoreUtil } = require('entrystore-js');
const filesOnly = process.argv.some(arg => arg === '--files'); // only pdf and png
const lstarg = process.argv[2];
const lst = config.lansstyrelser[lstarg];
if(!lst){
  console.error(`${lstarg} not found`);
  return 1;
}
console.log(lst);

if(![19,52,53].includes(lst.context)) {
  console.error(`you shouldn\'t remove stuff from context ${lst.context}`);
  return -1;
}


const es = new EntryStore(config.repository);
const esu = new EntryStoreUtil(es);

console.log(`logging in user ${config.user} to ${config.repository}, context ${lst.context}`);
es.getAuth().login(config.user, config.password).then(async () => {
  const context = es.getContextById(lst.context);
  const types = filesOnly ? [
    'http://schema.org/DigitalDocument',
    'http://schema.org/ImageObject'
  ] : [
    'http://schema.org/DigitalDocument',
    'http://schema.org/ImageObject',
    'http://data.lansstyrelsen.se/ns/Strategy',
    'http://data.lansstyrelsen.se/ns/Report',
    'http://data.lansstyrelsen.se/ns/Paper',
    'http://data.lansstyrelsen.se/ns/Brochure',
    'http://data.lansstyrelsen.se/ns/Newsletter',
    'http://purl.org/dc/dcmitype/Text',
  ];
 
  const promises = types.map((type) => {
     console.log(`removing ${type.substr(type.lastIndexOf('/')+1)}s from context ${lst.context}`);
     return esu.removeAll(
       es.newSolrQuery()
         .context(context)
         .rdfType(type)
     )});
 
  Promise.all(promises)
    .catch(err => console.log)
    .finally(()=> console.log('finished!'));
});
