const { getExtension } = require('../digitaldocument');

filenames = [
  'filil.html',
  'filil.med.flera.pubkter.jpg',
  'filil.aspx',
  'zip',
  'jpg',
  'filil.zip',
  '',
  'filil.pptx',
  'filil',
  'filil.xlsx',
  'filil.XLSX',
  'filil.XSLX',
  'filil.docX',
  'filil.pdf',
  'filil.jpeg',
  'filil.JPG',
  'filil.jpg',
  'filil.jped',
  'filil.asdfasdf/asdfasdf',
  'filil.asdfasdf/asd.asdf',
  'fililasdfasdf/asdfasdf',
  null,
  undefined
];

filenames.forEach(f => console.log(`${f}\t${getExtension(f)}`));

