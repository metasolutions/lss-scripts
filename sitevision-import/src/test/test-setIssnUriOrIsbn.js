const { setIssnUriOrIsbn } = require('./identifier');

pubs = [
  {issn: "1403-168X"},
  {issn: "140t3-168X"},
  {issn: "12-12-12-12"},
  {issn: "gurka"},
  {issn: ""},
  {issn: "ISSN 4444-4444"},
  {issn: "ISSN4444-4444"},
  {issn: "Issn4444-4444"},
  {issn: "ISBN 123-123-123"},
  {issn: "isbn 123-123-123"},
  {issn: "Isbn 123-123-123"},
  {issn: "ISBN123-123-123"},
  {issn: "4444-4444"},
  {issn: "444-4444"},
]


pubs = setIssnUriOrIsbn(pubs);

console.log(pubs);

