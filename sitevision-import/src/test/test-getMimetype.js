const {getMimetype, getExtension} = require('./digitaldocument');

filenames = [
  'fil.html',
  'fil.med.flera.pubkter.jpg',
  'fil.aspx',
  'zip',
  'jpg',
  'fil.zip',
  '',
  'fil.pptx',
  'fil',
  'fil.xlsx',
  'fil.XLSX',
  'fil.XSLX',
  'fil.docX',
  'fil.pdf',
  'fil.jpeg',
  'fil.JPG',
  'fil.jpg',
  'fil.jped',
  null,
  undefined
];

filenames.forEach(f => console.log(`${f}\t${getMimetype(getExtension(f))}`));
