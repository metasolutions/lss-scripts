const { EntryStore, promiseUtil } = require('entrystore-js');
const { 
  findPublicationsWithoutImage, 
  downloadPdf, 
  thumbnailPdf, 
  createAndCommitThumbnailEntry, 
  uploadPngToThumbnailEntry, 
  addImagePropertyToPublicationEntry
} = require("../addPng.js");

const config = require('../config'); // the config file includes values for repo, context, username et.c.
const es = new EntryStore(config.repository); 
const lst = {context: 19};

es.getAuth().login(config.user, config.password).then( async () => {
  const context = es.getContextById(lst.context);

  publicationsWithoutImage = await findPublicationsWithoutImage(es, context)
    .catch(err => console.error(`Could not get publications: ${err.message}`));
  
  console.log(`found ${publicationsWithoutImage?.length} imageless pubs in context ${lst.context}`);
  
  //TODO: ta bort begränsningen, sliceningen
  publicationsWithoutImage = publicationsWithoutImage.slice(0, 3);  
  console.log(`sliced to ${publicationsWithoutImage?.length} imageless pubs`);

  // let's go
  promiseUtil.forEach(publicationsWithoutImage, async pub => {
    const publicationEntryId = pub.getId();
    console.log(`getting publication ${publicationEntryId}`); 
    await downloadPdf(pub)
     .catch(err => console.log(`download error: ${publicationEntryId}`, err));

    console.log(`thumbnailing ${publicationEntryId}`); 
    await thumbnailPdf(pub)
      .catch(err => console.error(`thumbnail error: ${publicationEntryId}`, err))

    console.log(`create entry for ${publicationEntryId}`); 
    const thumbnailEntry = await createAndCommitThumbnailEntry(context, publicationEntryId)
      .catch(err => console.error(`error on commit image entry for ${publicationEntryId}`, err));

    console.log(`upload thumbnail for ${publicationEntryId}`); 
    await uploadPngToThumbnailEntry(pub, thumbnailEntry)
      .catch(err => console.error(`error on upload entry for ${publicationEntryId}`, err));

    console.log(`connect entries: ${thumbnailEntry.getResourceURI()} to ${publicationEntryId}`);
    await addImagePropertyToPublicationEntry(pub, thumbnailEntry.getResourceURI())
      .catch(err => { console.error(`faan å: `, err);})
 });

}).catch(err => console.error(`Computer says no:`, err));
