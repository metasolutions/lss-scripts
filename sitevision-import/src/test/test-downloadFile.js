const { downloadFile } = require('../digitaldocument.js');
const { downloadPfd } = require('../addPng.js');

(async () => {
  console.log(`downloading with downloadFile()`);
  console.log(downloadFile);
  await downloadFile('https://catalog.lansstyrelsen.se/store/34/resource/DAC_2020__11', 'downloads/DH_2020__11.pdf');
  await downloadFile('https://catalog.lansstyrelsen.se/store/34/resource/DAC_2020__12', 'downloads/DH_2020__12.pdf');

})();

