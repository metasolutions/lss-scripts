const config = require('./config');
const extractLinks = require('./extractLinks');
const getPublications = require('./getPublications');
const getPaths = require('./getPaths');
const { promiseUtil, EntryStore } = require('entrystore-js/dist/EntryStore.node');
const es = new EntryStore(config.repository);
const fs = require('fs');
const im = require('imagemagick');

const problems = [];

const weirdChars = str => str.replace(/å/g, 'Ж').replace(/ä/g, 'Д').replace(/ö/g, 'Ф');

es.getAuth().login(config.user, config.password).then(async () => {
  const context = es.getContextById(config.context);

  const paths = await getPaths();
  const generatePNG = async (link, id) => {
    const webPath = link.attributes.href.substr(1);
    let filePath = paths[webPath];
    if (filePath) {
      filePath = weirdChars(filePath);
      try {
        const euri = es.getEntryURI(config.context, id);
        console.log(`Generating PNG from ${filePath} and importing into ${id}`);
        return es.getEntry(euri).catch((err) => {
          return context.newEntry(id)
            .addL('dcterms:title', id)
            .add('rdf:type', 'http://schema.org/ImageObject')
            .commit();
        }).then((pdfEntry) => {
          if (!pdfEntry.getEntryInfo().getSize()) { // Check if it has been uploaded already
            return new Promise((resolve, reject) => {
              const pngPath = `${filePath}.png`;
              im.convert(['-thumbnail', '450x', '-background', 'white', '-alpha', 'remove', `${filePath}[0]`, pngPath], () => {
                const resource = pdfEntry.getResource(true);
                resource.putFile(pngPath, 'image/png').then(resolve, (err) => {
                  console.log(`=======Could not load ${pngPath}`);
                  problems.push({ id, pngPath });
                  reject(err);
                });
              });
            });
          }
        });
      } catch(err) {
        console.log(`=======Could not load ${filePath}`);
        problems.push({ id, filePath });
        return Promise.resolve();
      }
    }
    return Promise.resolve();
  };

  const publications = await getPublications();
  console.log(`Found ${publications.length} publications`);

  let nr = 0;
  promiseUtil.forEach(publications, async (r) => {
    const id = r.id;
    const row = r.row;const promises = [];

    if (row.Content1 !== '') {
      const links = extractLinks(row.Content1);
      if (links.length > 0) {
        await generatePNG(links[0], `${id}_png`);
        nr += 1;
      }
    }
  }).then(() => {
    console.log('Imported: '+nr);
    console.log('=====Had the following problems========:');
    problems.forEach((p) => {
      console.log(`Could not import ${p.filePath} into ${p.id}`);
    });
  }, (err) => {
    console.log(err);
  });
});