const im = require('imagemagick');

// const lstarg = process.argv[2]?.toLowerCase();
// const lst = config.lansstyrelser[lstarg] || 'thawalleys';
// if(!lst){
//   console.error(`länsstyrelse ${lstarg} not found`);
//   return 1;
// }
// 
// const es = new EntryStore(config.repository); // TODO initiate somewhere else
// 
// const start = (config, lst) => {
//   console.log(`logging in ${config.user} to context ${lst.context}:`);
//   es.getAuth().login(config.user, config.password).then( async () => {
//     const context = es.getContextById(lst.context);
//     .
//     .
//     .
//   }).catch(err => console.error(`Computer says no: ${err}`));
// }

const getImageEntryId = mainEntryId => `I${mainEntryId}`;

const publicationTypes = [
  "http://data.lansstyrelsen.se/ns/Report",
  "http://data.lansstyrelsen.se/ns/Paper",
  "http://data.lansstyrelsen.se/ns/Newsletter",
  "http://data.lansstyrelsen.se/ns/Brochure",
  "http://data.lansstyrelsen.se/ns/Strategy",
  "http://data.lansstyrelsen.se/ns/Text",
]

const findPublicationsWithoutImage = async (es, context) => {
  const errors = [];
  const entriesToReturn = [];
  const allEntries = es.newSolrQuery()
    .context(context)
    .rdfType(publicationTypes);

  await allEntries.forEach(async e => {
    const entryMetadata = e.getMetadata();
    const imageResourceUri = entryMetadata.findFirstValue(e.getResourceURI(), 'schema:image');
    if(imageResourceUri){
      const imageEntryUri = es.getEntryURIFromURI(imageResourceUri);
      const imageEntry = await es.getEntry(imageEntryUri)
        .catch(err => {
          errors.push(err);
        })

      if(!imageEntry?.getEntryInfo().getSize()){
        entriesToReturn.push(e);
      }
    } else {
      entriesToReturn.push(e);
    }
  });
  return Promise.resolve(entriesToReturn);
}

const downloadFile = async (url, filename) => {
  const axios = require('axios');
  const fs = require('fs');
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(filename);
    axios({
      url: url,
      method: "GET",
      responseType: 'stream'
    }).then(response => {
      response.data.pipe(file);
      resolve(filename);
    }).catch(err => {
      console.error('Error in axios GET');
      reject(err);
    })
  })
}

const downloadPdf = async pub => {
  const localPdfFilename = `downloads/${pub.getId()}.pdf`;
  const pdfURI = pub.getMetadata().findFirstValue(pub.getResourceURI(), 'dcterms:hasVersion');
  if(pdfURI){
    console.log(`downloading ${pdfURI} to ${localPdfFilename}`)
    return downloadFile(pdfURI, localPdfFilename)
      .catch(err => Promise.reject(`COuld not download pdf for ${pub.getId()}: ${err}`));
  }
  else 
    return Promise.reject(`No uri found for ${pub.getId()}`);
}

// const downloadPdf = async (es, pub) => {
//   const localPdfFilename = `downloads/${pub.getId()}.pdf`;
//   // const file = fs.createWriteStream(localPdfFilename);
//   // hm, ger getREST.get en ström? eller nåt annat?
//   const pdfURI = pub.getMetadata().findFirstValue(pub.getResourceURI(), 'dcterms:hasVersion');
//   console.log(pdfURI);
//   const data = await es.getREST().get(pdfURI)
//     .catch(err => {
//       console.error('knas:', err);
//       return Promise.reject(err);
//     })
//   console.log(`data: ${data}`, data);
// }

const thumbnailFirstPage = async (fromFilename, toFilename) => {
  // TODO rewrite, return promise first?
  const firstPage = `${fromFilename}[0]`;
  im.convert(['-thumbnail','450x', '-background', 'white', '-alpha', 'remove', firstPage, toFilename], function(err, stdout){
    if (err) return Promise.reject(err);
    else return Promise.resolve(`Result: ${stdout}`);
  });
}

const thumbnailPdf = async pub => {
  const localPdfFilename = `downloads/${pub.getId()}.pdf`;
  const thumbnailFilename = `downloads/${getImageEntryId(pub.getId())}.png`;
  return thumbnailFirstPage(localPdfFilename, thumbnailFilename)
}

const createAndCommitThumbnailEntry = async (context, mainEntryId) => {
  const thumbnailEntryId = getImageEntryId(mainEntryId);
  const title = thumbnailEntryId;
  const newThumbnailEntry = context.newEntry(thumbnailEntryId)
    .add('rdf:type', 'http://schema.org/ImageObject')
    .addL('dcterms:title', title);
  return newThumbnailEntry.commit()
    .catch(err => {
      if(err.status == 409){
        console.log('commit failed because of conflict. committin new metadata to befintlig entry');
        // ERROR: härifrån verkar jag få tillbaka en http-reponse iställte för en entry
        return newThumbnailEntry.commitMetadata()
          .catch(err => {
          console.error(`commitf ailed because of error ${err.status}`);
          return Promise.reject(err);
        });
      } else {
        console.error(`commitf ailed because of error ${err.status}`);
        return Promise.reject(err);
      }
    });
};

const uploadPngToThumbnailEntry = async (pub, thumbnailEntry) => {
  if(thumbnailEntry.getEntryInfo()?.getSize()) return Promise.reject(`File already uploaded for ${thumbnailEntry.getId()}`);
  const thumbnailFilename = `downloads/${getImageEntryId(pub.getId())}.png`;
  const fs = require('fs');
  if(!fs.existsSync(thumbnailFilename)) return Promise.reject(`File ${thumbnailFilename} not found`)

  const mimetype = 'image/png';
  console.log(`uploading ${thumbnailFilename} to ${thumbnailEntry.getId()}`);
  const file = fs.createReadStream(thumbnailFilename);
  const resource = await thumbnailEntry.getResource(true)
  console.log('res uri:', resource.getResourceURI());
  resource.putFile(file, mimetype)
    .then(res => {
      return Promise.resolve(res);
    })
    .catch(err => {
      console.log(`failed to put file ${thumbnailFilename} to entry ${thumbnailEntry.getId()}:`, err);
      return Promise.reject(err);
    });
}

const addImagePropertyToPublicationEntry = async (publicationEntry, imageEntryUri) => {
  publicationEntry.add('schema:image', imageEntryUri);
  return publicationEntry.commitMetadata();
}

module.exports = {
  findPublicationsWithoutImage,
  downloadPdf,
  thumbnailPdf,
  createAndCommitThumbnailEntry,
  uploadPngToThumbnailEntry,
  addImagePropertyToPublicationEntry
}
