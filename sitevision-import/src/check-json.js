const config = require('./config'); // the config file includes values for repo, context, username et.c.
const readline = require('readline-sync');
const fs = require('fs');
const {getExtension} = require('./digitaldocument');


validTypes = ["strategy", "report", "other", "brochure", "paper", "newsletter", "Planeringsunderlag", "other"];

Array.prototype.sum = function() {
  const array_sum = function(my_array) {
    if (my_array.length === 1) {
      return my_array[0];
    }
    else {
      return my_array.pop() + array_sum(my_array);
    }
  };
  return array_sum(this);
}

Array.prototype.uniq = function() {
  seen = [];
  this.forEach(el => { seen.includes(el) || seen.push(el) });
  return seen;
}

Array.prototype.dubletter = function() {
  seen = {}
  this.forEach(el => {
    if(Object.keys(seen).includes(el)){
      seen[el] += 1;
    } else {
      seen[el] = 1;
    }
  });
  return Object.keys(seen).filter(key => seen[key] > 1);//seen[key] > 2);
}


const idOrTitle = post => (post.identifier || post.year) + `: ${post.title.substring(0,30)}`;
const idConform = str => str && str.replace(/[\.:-]/, '_');
const validId = str => str && idConform(str).match(/\d{4}_\d+/);
const getFileTypes = post => {
  const types = [];
  if(post.file) types.push(post.file.split('.').pop());
  post.appendix.forEach(apx => types.push(apx.split('.').pop()));
  return types.uniq().filter(t => t.length < 8);
}
// here it all starts:
// const json = require(config.inputfile);

// länsstyrelse
// antal dokument
// antal utan id
// antal ogiltigt id
// antal utan fil-url
// antal utan titel
// olika typer
// antal utan typ
// antal utan ämne
// antal med appendix

let json = [];
const files = fs.readdirSync('../data/').filter(f => f.endsWith('.json'));
files.forEach(f => {
  json = json.concat(require(`../data/${f}`));
})

logfile = [];

const publishers = json.map(pub => pub.publisher).uniq();
console.log(publishers);


publishers.forEach(publisher => {
  org = json.filter(pub => pub.publisher === publisher);

  orglog = {};
    
    orglog["Antal publikationer"] = org.length;
    orglog["Antal utan id"] = org.filter(post => !post.identifier).length;
    const felaktigaId =org.map(post => post.identifier?.length).uniq().filter(identifier => !!identifier).sort(); 
    orglog["Antal felaktiga id"] = felaktigaId.length;
    orglog["Felaktiga id"] = felaktigaId;
    const idDubletter = org.map(
      post => post.identifier && 
      post.identifier.length && 
      validId(post.identifier) && 
      idConform(post.identifier)
    ).filter(identifier => !!identifier).dubletter().sort();
    orglog["Antal ID-dubletter"] = idDubletter.length;
    orglog["ID-dubletter"] = idDubletter;
    orglog["Ogiltiga typer"] = org.map(post => post.type).uniq().sort().filter(type => !validTypes.includes(type)).toString() || "inga";
    orglog["Utan fil"] = org.filter(post => !post.file || post.file.length < 12).map(post => idOrTitle(post));
    orglog["Med appendix"] = org.filter(post => post.appendix.length).map(post => `${post.identifier}: ${post.title.substring(0, 30)}`);

    const antaldokument = org.filter(post => post.file && post.file.length >= 12).length

    const antalappendices = org.map(post => post.appendix.length).sum();

    orglog['Antal dokument'] = antaldokument;
    orglog['Antal appendices'] = antalappendices;
    orglog['Antal filer totalt'] = antaldokument + antalappendices;

    org.map(post => orglog["filtyper"] = (orglog["filtyper"] || []).concat(getFileTypes(post)).uniq());
    if(orglog['filtyper'].length > 1) org.forEach(post => {
      orglog["udda filer"] = org.filter(post => post.file?.split('.')?.pop() !== 'pdf').map(post => post.file);

      apxs = [];
      postsWithAppendices = org.filter(post => post.appendix.length);
      postsWithAppendices.forEach(post => 
        post.appendix.forEach(apx => { 
          if(apx && !apx.includes('pdf')) {
            apxs.push(apx);
          }
      })
    )
    // orglog['udda filtyper'] = (orglog['udda filtyper']||[]).concat(apxs);
   // console.log(apxs);
  });

  logfile.push({publisher: publisher, log: orglog});
})

//const char5s = json.map(post => post.identifier && post.identifier[4]).uniq().sort().toString();
// logfile.forEach(post => console.log(post.publisher["Utan svid"]))

logfile.forEach(post => console.log(post));
//logfile.forEach(post => console.log(post.log['type']));
//const apxposts = json.filter(p => p.appendix && p.appendix.length > 0);
//console.log(apxposts.length, apxposts[0]);

const filtyper = logfile.map(org => org.log['filtyper']).flat().uniq();
console.log({"filtyper": filtyper});
