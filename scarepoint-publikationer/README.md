# Scripts for Länsstyrelsen

There are three main scripts:

- import.js for importing / creating entries for publications
- importPDFs.js for importing corresponding PDFs
- generatePNGs.js for creating screenshoots for all PDFs

To run the import scripts (the others are run the same):

    cd src
    node import.js

Before you can run the scripts you have to:
 
1. run yarn to get node_modules
2. provide a config.js in the src directory, make a copy of config.js
and provide username and password therein
3. Point to where the PDFs are located. You do that by providing a
soft link in the src directory to the "Publications" folder in the dumped
archive. The soft link must be named exactly "Publications"