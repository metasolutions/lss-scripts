const config = require('./config'); // the config file includes values for repo, context, username et.c.
const extractLinks = require('./extractLinks'); // utility to extract links from html field
const getPublications = require('./getPublications'); // utility to read posts from csv file
const LogModule = require('./log'); // for logging events
const log = LogModule('import');
const { promiseUtil, EntryStore, namespaces } = require('entrystore-js/dist/EntryStore.node'); // EntryStore and utilities from entrystore.js
const { reject } = require('lodash');
const es = new EntryStore(config.repository);

// commandline arguments
const dryrun = process.argv.some(arg => arg === '--dry-run'); // do not insert in entrystore, only create log
const createIds = process.argv.some(arg => arg === '--create-ids'); // a dcterms:identifier will be created, otherwise post will be skipped

const lstLetter = config.publisher.substring(config.publisher.lastIndexOf('/')+1);

if(dryrun) log.info("Warning: functionality for dry-run is not implemented yet. Data _will_ be inserted in Entrystore!");
log.debug(`dryrun: ${dryrun}, createId: ${createIds}`);

namespaces.add('adms', 'http://www.w3.org/ns/adms#');
namespaces.add('schema', 'http://schema.org/');
namespaces.add('lss', 'http://data.lansstyrelsen.se/ns/');

/* sätt rdf-type utifrån önskad rangordning.
  // I det gamla systemet kan ett dokument ha flera typer, 
  // men nu ska det ha ett, valt utifrån följande rangordning.
  // Typen Planeringsunderlag ska bytas ut mot Rapport, och 
  // dokumentet ska i dessa fall markeras.
    1. Strategi
    2. Rapport
    3. Tidining
    4. Broschyrer/foldrar
    5. Nyhetsbrev
    6. Övriga publikationer
*/
const getRdfType = publication => {
  let rdfType = 'http://data.lansstyrelsen.se/ns/';
  const rdfTypeOvriga = 'http://purl.org/dc/dcmitype/Text';
  if(!(publication && publication.DocTypePublications)){
    log.info(`${publication.PublicationNumber} has no rdfType. Set to ${rdfTypeOvriga}`);
    return {type: rdfTypeOvriga};
  }

  const types = publication.DocTypePublications.split(';#');
  types.shift();
  types.pop();

  const hasType = requestedType => types.some(existingType => existingType === requestedType);

  if(hasType('Strategier')) rdfType += 'Strategy';
  else if(hasType('Rapporter')) rdfType += 'Report';
  else if(hasType('Planeringsunderlag')) { // should be Report, but have different publication channel
    rdfType += 'Report';
    return {type: rdfType, wasPlaneringsunderlag: true}
  }
  else if(hasType('Tidning')) rdfType += 'Paper'
  else if(hasType('Broschyrer/foldrar')) rdfType += 'Brochure'
  else if(hasType('Nyhetsbrev')) rdfType += 'Newsletter'
  else if(hasType('Övriga publikationer')) rdfType = rdfTypeOvriga;
  return {type: rdfType};
}

log.info(`logging in user ${config.user} to ${config.repository}, context ${config.context}`);
es.getAuth().login(config.user, config.password).then(async () => {
  const context = es.getContextById(config.context);
  const subjectIdx = {};

  const subjContext = es.getContextById(config.subjectContext);

  log.info('fetching resource URI:s ...');
  await es.newSolrQuery().context(subjContext).rdfType('skos:Concept').forEach((conceptEntry) => {
    const resourceUri = conceptEntry.getResourceURI();
    subjectIdx[conceptEntry.getMetadata().findFirstValue(resourceUri, 'skos:prefLabel').toLowerCase()] = resourceUri;
  });

  log.info(`getting publications from data dir ...`);
  const publicationList = await getPublications(
    {
      inputFile: config.inputfile,
      log: log,
      createIds: createIds, // if true, creates IDs for publications with no ID 
    }
  );
  log.info(`read ${publicationList.length} publication posts from data dir. Starting the import ...`);

  let nr = 0; // will count the number of imported reports from csv file
  promiseUtil.forEach(publicationList, (row) => {
    const { id, publication } = row;
    if(!publication){
      reject(`no data for id '${id}'. Skipping.`);
    }

    const rdfType = getRdfType(publication).type;

    let newEntry = context.newEntry(`_${id}`)
      .add('rdf:type', rdfType)
      .addL('dcterms:title', publication.PageTitle)
      .add('dcterms:publisher', config.publisher)
      .addL('dcterms:identifier', `${lstLetter}:${publication.PublicationNumber}`);

    if(publication.JournalNumber != "") newEntry.addL('adms:identifier', publication.JournalNumber);

    if (publication.CategoryArea != "") {
      publication.CategoryArea.split(';').map((pair) => pair.split('|')[0]).forEach((s) => {
        const uri = subjectIdx[s.trim().toLowerCase()];
        if (uri) {
          newEntry.add('dcterms:subject', uri);
        } else if (s.indexOf('&') != -1 || s.indexOf('＆') != -1) {
          s.split(/[＆&]/).forEach(part => {
            const partUri = subjectIdx[part.trim().toLowerCase()];
            if (partUri) {
              newEntry.add('dcterms:subject', partUri);
            }
          });
        } else if (s.indexOf('och') != -1) {
          s.split('och').forEach(part => {
            const partUri = subjectIdx[part.trim().toLowerCase()];
            if (partUri) {
              newEntry.add('dcterms:subject', partUri);
            }
          });
        }
      });
    }

    if (publication.PublicationYear != "") {
      newEntry.addD('dcterms:issued', publication.PublicationYear, 'xsd:date');
    }

    if (!!publication.MetaKeywords) {
      publication.MetaKeywords.split(',').forEach((kw) => {
        newEntry.addL('dcterms:subject', kw.trim(), 'sv');
      });
    }

    if (publication.ISBNNumber != "") {
      newEntry.add('schema:issn', `http://issn.org/resource/ISSN/${publication.ISBNNumber}`);
    }

    if (publication.Summary != '') {
      newEntry.addL('dcterms:abstract', publication.Summary);
    }

    newEntry.add('schema:image', es.getResourceURI(config.context, `${id}_png`));

    if (publication.Content1 != '') {
      const links = extractLinks(publication.Content1);
      if (links.length > 0) {
        newEntry.add('dcterms:hasVersion', es.getResourceURI(config.context, id));
        links.shift();
        links.forEach((l, idx) => {
          newEntry.add('dcterms:hasPart', es.getResourceURI(config.context, `${id}_part_${idx+1}`));
        });
      }
    }

    // if the original rdf type was 'Planeringsunderlag', 
    // the rdfType will have been changed to 'Report' for the new entry, 
    // The new entry should then also have a different lss channel (publiceringskanal)
    if(rdfType.wasPlaneringsunderlag){
      newEntry.add('lss:channel', 'https://catalog.lansstyrelsen.se/store/12/resource/2');
      log.info(`${id} imported as report with lss:channel resource/2`);
    }
    nr++;

    log.debug(`importing ${nr} of ${publicationList.length} with id ${id} as ${rdfType.type}`);

    // return the promise for the commit of the new entry
    return newEntry.commitMetadata().catch(() => newEntry.commit());
  })
  .then(() => {
    log.info(`imported ${nr} publications`);
  })
  .catch(err => {
    log.error(err);
  })
}).catch((err)=> console.log(`Computer says no: ${err}`));
