const Papa = require('papaparse');
const fs = require('fs');

module.exports = function(options) {
  const {inputFile, log, createIds} = options;
  const input = fs.readFileSync(inputFile, {encoding: 'utf-8'});
  const publications = []; // the array of publications to be returned 
  const pubIds = []; // håller reda på om ett publikations-ID är medräknat

  // count publications grouped by publication year
  const pubCount = []; 
  const generatePublicationNumber = year => {
    if(pubCount[year] === undefined) pubCount[year] = 0;
    pubCount[year] += 1;
    return `${year}:_${pubCount[year]}`;
  }

  const checkForConflict = (id, row) => {
    id = id.replace(':', '_');
    // check if there's an id conflict, add row or skip
    if (pubIds[id]) {
      if(pubIds[id].some(t => t === row.PageTitle)){
        log.idConflict(`${row.PublicationNumber} with title '${row.PageTitle}': already imported with same title. Assuming duplicate. Skipping.`);
      } else {
        log.idConflict(`${row.PublicationNumber} with title '${row.PageTitle}': already imported as '${pubIds[id].join(', ')}'`);
        pubIds[id].push(row.PageTitle); // save title for later comparison in case of id conflict
      }
    } else {
      publications.push({id, publication: row}); 
      pubIds[id] = [row.PageTitle];  // save title for later comparison in case of id conflict
    }

  }

  return new Promise(resolve => {
    Papa.parse(input, {
      delimiter: ',', //'¤', // the field delimiter in the csv
      header: true,   // the csv file has a header row
      complete: (results) => {
        log.info(`Rows found: ${results.data.length}`);
        const guessings = [];

        results.data.forEach((row) => {
          row.PageTitle = row.PageTitle ? row.PageTitle.replace(/(\r\n|\n|\r)/gm, ""):''; 

          if(row.PublicationYear){
            row.PublicationYear = row.PublicationYear.trim();
            if(row.PublicationYear.match(/^\d{4}$/) === null){
              log.badIdFormat(`publication with id '${row.PublicationNumber}' has an invalid year: '${row.PublicationYear}'`);
              return;
            }
          }

          // if publication number is missing, create one or skip row
          if (row.PublicationNumber == '' || !row.PublicationNumber) {  
            if(createIds === true){
              if(!row.PublicationYear){
                log.noId(`no id and no publication year, ignoring row with title '${row.PageTitle}'`);
                return;
              }
              row.PublicationNumber = generatePublicationNumber(row.PublicationYear);
              log.info(`generated id ${row.PublicationNumber} for publication with title '${row.PageTitle}'`);
            } else {
              log.noId(`ignoring row with title '${row.PageTitle}'`);
              return;
            }
          }
          log.debug(`row.PublicationNumber: ${row.PublicationNumber}`);

          // checks format of publication number/id
          row.PublicationNumber = row.PublicationNumber.trim();
          id = row.PublicationNumber;
          id = id.replace('-', ':');
          // id = id.replace('_', ':'); // TODO: this conflicts with the new requirement of a _ after colon in generated publication numbers. Is it needed?
          if (id.match(/^\d+:_?\d+.?$/) === null) {
            // test if a valid id can be constructed by concatinating PublicationYear and id
            const test = `${row.PublicationYear}:${id}`;
            if(id.match(/^\d+$/) != null && test.match(/^\d+:_?\d+.?$/) != null){
                originalId = id;
                id = test;
                row.PublicationNumber = test;
                guessings.push({originalId, id, row});
                return;
            } else {
              if(!row.PublicationYear){
                log.badIdFormat(`could not create id, no publication year, ignoring row with title '${row.PageTitle}'`);
                return;
              }
              id = generatePublicationNumber(row.PublicationYear);
              log.info(`generated id ${id} for publication with original id ${id} and title '${row.PageTitle}'`);              
            }
          }
          checkForConflict(id, row);
        });

        guessings.forEach(guess => {
          log.info(`guessing id ${guess.id} for publication with original id ${guess.originalId}, year ${guess.row.PublicationYear} and title ${guess.row.PageTitle}`);
          checkForConflict(guess.id, guess.row);
        })

        resolve(publications);
      }});
  });
};