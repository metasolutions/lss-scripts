const htmlParser = require('node-html-parser');

const findAElements = (node) => {
  const matches = [];
  const recurse = (n) => {
    if (n.tagName === 'a') {
      matches.push(n);
    }
    if (n.childNodes) {
      n.childNodes.forEach(recurse);
    }
  };
  recurse(node);
  return matches;
};

module.exports = function(str) {
  const root = htmlParser.parse(str);
  return findAElements(root);
};