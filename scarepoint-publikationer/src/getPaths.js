const Papa = require('papaparse');
const fs = require('fs');

module.exports = function() {
  return new Promise(resolve => {
    const pathsSource = fs.readFileSync('../data/paths.csv', {encoding: 'utf-8'});
    Papa.parse(pathsSource, {
      delimiter: ',',
      header: true,
      complete: function (pathResults) {
        const paths = {};
        pathResults.data.forEach((row) => {
          const p = row.diskFilePath.substr(8).replace(/\\/g, '/');
          paths[row.webFilePath] = p;
        });
        resolve(paths);
      }
    });
  });
};
