const config = require('./config');
const {EntryStore, EntryStoreUtil } = require('entrystore-js/dist/EntryStore.node');
const filesOnly = process.argv.some(arg => arg === '--files'); // only pdf and png

const es = new EntryStore(config.repository);
const esu = new EntryStoreUtil(es);

console.log(`logging in user ${config.user} to ${config.repository}, context ${config.context}`);
es.getAuth().login(config.user, config.password).then(async () => {
  const context = es.getContextById(config.context);
  const types = filesOnly ? [
    'http://schema.org/DigitalDocument',
    'http://schema.org/ImageObject'
  ] : [
    'http://data.lansstyrelsen.se/ns/Strategy',
    'http://data.lansstyrelsen.se/ns/Report',
    'http://data.lansstyrelsen.se/ns/Paper',
    'http://data.lansstyrelsen.se/ns/Brochure',
    'http://data.lansstyrelsen.se/ns/Newsletter',
    'http://purl.org/dc/dcmitype/Text',
  ];

  const promises = types.map((type) => {
    console.log(`removing ${type.substr(type.lastIndexOf('/')+1)}s from context ${config.context}`);
    return esu.removeAll(
      es.newSolrQuery()
        .context(context)
        .rdfType(type))
  });

  Promise.all(promises)
    .catch(err => console.log)
    .finally(()=> console.log('finished!'));
});
