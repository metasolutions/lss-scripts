const config = require('./config'); // the config file includes values for repo, context, username et.c.
const extractLinks = require('./extractLinks'); // utility to extract links from html field
const getPublications = require('./getPublications');  // utility to read posts from csv file
const LogModule = require('./log');// for logging events
const log = LogModule('pdf-import');
const { promiseUtil, EntryStore } = require('entrystore-js/dist/EntryStore.node');
const es = new EntryStore(config.repository);
const fs = require('fs'); // file system utils from Node.js
const im = require('imagemagick');

// commandline arguments
const createIds = process.argv.some(arg => arg === '--create-ids'); // a dcterms:identifier will be created, otherwise post will be skipped
const dryrun = process.argv.some(arg => arg === '--dry-run'); // do not insert in entrystore, only create log
if(dryrun) log.info("Warning: functionality for dry-run is not implemented yet. Data _will_ be inserted in Entrystore!");

// helper function for converting wierd chars that some filenames have in them
const weirdChars = str => str.replace(/å/g, 'Ж').replace(/ä/g, 'Д').replace(/ö/g, 'Ф');

// helper function for extracting href from anchor 
// const getLinkHref = link => {
//   let linkHref = link.attributes.href.substr(1);
//   if (linkHref) {
//     linkHref = linkHref.toLowerCase();
//     linkHref = linkHref.replace(/%20/g, " ");
//   } else throw `no href in link '${link}'`;
//   return linkHref;
// }

// helper function to lowercase and check file path
const fixFilePath = path => {
  path = `${config.pdfDirectory}/${path}`.toLowerCase().replace(/%20/g, " ");
  if (!fs.existsSync(path)){
    const test = weirdChars(path);
    if (!fs.existsSync(test))
      throw `missing file: ${path}`;
    else path = test;
  }
  if(!fs.lstatSync(path).isFile())
    throw `path is not a file: ${path}`;
  return path;
}


log.info(`logging in user ${config.user} to ${config.repository}, context ${config.context}`);
es.getAuth().login(config.user, config.password).then(async () => {
  const context = es.getContextById(config.context);

  const storeFile = async (id, filePath) => {
    try {      
      const euri = es.getEntryURI(config.context, id);
      // log.debug(`Importing document ${filePath}\n\tfor linkHref ${linkHref}\n\tinto ${euri}`);
      return es.getEntry(euri).catch((err) => {
        log.info(`id ${id}: creating entry for ${euri}`);
        return context.newEntry(id)
          .addL('dcterms:title', id)
          .add('rdf:type', 'http://schema.org/DigitalDocument')
          .commit();
      }).then((pdfEntry) => {
        if (!pdfEntry.getEntryInfo().getSize()) { // upload only if there isn't already a file
          log.info(`id ${id}: putting file ${filePath}`);
          const resource = pdfEntry.getResource(true);
          return es.getREST().put(
            resource.getResourceURI(),
            fs.readFileSync(filePath), 
            null, 
            'application/pdf'
          ).catch((err) => {
            log.error(`${err.errno}: id:${id} - ${filePath} `);
          });
        } else {
          log.debug(`id ${id}: resource already present for ${euri}`);
        }
      });
    } catch(err) {
      log.error(`id ${id}: could not load ${filePath}`);
      return Promise.resolve();
    }
  }

  const generatePng = async(id, filePath) => {
    id = `${id}_png`;
    log.debug(`id: ${id}: png generation for ${filePath}`);
    const euri = es.getEntryURI(config.context, id);
    let entry;
    entry = await es.getEntry(euri)
      .catch((err) => {
        log.debug(`id: ${id}: creating entry for ${euri}`);
        return context.newEntry(id)
          .addL('dcterms:title', id)
          .add('rdf:type', 'http://schema.org/ImageObject')
          .commit()
          .catch((err) => log.entryFailure(`id: ${id}: failed to create entry - ${err}`)); 
      });

    if (entry.getEntryInfo().getSize()) { // Check if it has been uploaded already
      log.debug(`${id}: png is already uploaded`);
    } else {  
      log.debug(`id ${id}: generating png ...`);
      // create a png
      const pngPath = `${filePath}.png`;
      await im.convert(['-thumbnail', '450x', '-background', 'white', '-alpha', 'remove', `${filePath}[0]`, pngPath], (err, stdout) => {if(err) log.error(err)});
      if(fs.existsSync(pngPath))
        log.debug(`id ${id}: creation succeeded: ${pngPath}`);
      else{
        throw(`id ${id}: png creation failed ${pngPath}`);
      }
      // upload the png
      const resource = entry.getResource(true);
      await es.getREST().put(
        resource.getResourceURI(),
        fs.readFileSync(pngPath),
        null,
        'image/png'
      ).catch((err)=>{
        log.uploadError(`id ${id}: could not upload ${pngPath}`);
        log.debug(err);
      })
      log.info(`id ${id}: uploaded ${pngPath}`);
    }
  }

  const publications = await getPublications(
    {
      inputFile: config.inputfile,
      log: log,
      createIds: createIds,
    }
  );
  log.info(`Found ${publications.length} publications`);

  let numberOfImportedFiles = 0;
  promiseUtil.forEach(publications, async (row) => {
    const {id, publication} = row;

    if (publication.Content1 !== '') {
      const links = extractLinks(publication.Content1);
      let filePaths;
      try {
         filePaths = publication.FilePaths.split(';');
      } catch (err){
        log.error(err);
        console.log(publication);
        return Promise.resolve();
      }
      
      if(links.length !== filePaths.length) { 
        log.error(`id ${id}: ${links.length} links but ${filePaths.length} filepaths`);
        log.debug(`id ${id}: ${publication.Content1}`);
        log.debug(`id ${id}: ${publication.FilePaths}`);
      }

      if (links.length > 0) {
        try {
          const path = fixFilePath(filePaths[0])

          // store main file
          await storeFile(id, path).catch((err)=>{log.error(`${err}`)});
          numberOfImportedFiles += 1;

          // generate png thumbnail
          await generatePng(id, path);
          
          // store parts if present
          links.shift();
          filePaths.shift();
          let idx = 0;
          await promiseUtil.forEach(links, async(link) => {
            idx += 1;
            // const linkHref = getLinkHref(link);
            const partId = `${id}_part_${idx}`;
            const path = fixFilePath(filePaths[idx-1]);
            log.debug(`id ${id} part: ${idx}: ${path}`);
            await storeFile(partId, path);
            return await generatePng(partId, path);
          }).catch(err => {throw(err)});
        } catch (err){
          log.error(`id ${id}: ${err}`);
          return Promise.resolve();
        }
      }
    }
  }).then(() => {
    log.info(`Imported files: ${numberOfImportedFiles}`);
  }).catch(err => {log.error(`in promiseUtil.forEach: ${err}`)});
}).catch((err)=> console.log(`Computer says no: ${err}`));;
