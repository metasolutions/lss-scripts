# makes dirs writable (for imagemagick)
#find $1 -type d -exec chmod a+w {} \;

# lowercase all files and dirs
for SRC in `find $1 -depth`
do
     DST=`dirname "${SRC}"`/`basename "${SRC}" | tr '[A-Z]' '[a-z]'`
     if [ "${SRC}" != "${DST}" ]
     then
	echo "${SRC} --> ${DST}"
         [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "ERROR: ${SRC} was not renamed"
     fi
done


